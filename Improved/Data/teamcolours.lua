team_colours =
{
	{	-- self
		colour = { 255, 255, 255 },
		tint = 0,
		badge="Wolfhead.tga"
	},
	{	-- ally
		colour = { 255, 255, 255 },
		tint = 0,
		badge="Wolfhead.tga"
	},
	{	-- enemy
		colour = { 255, 255, 255 },
		tint = 0,
		badge="Wolfhead.tga"
	},
	{	-- neutral
		colour = { 255, 255, 255 },
		tint = 0,
		badge="Wolfhead.tga"
	},
}


player_colours =
{
	{
		--Current Player (blue)		
		colour = { 170, 170, 170 },
		tint = 0,
		badge="Wolfhead.tga"
	},
	{
		--Allied Player 1 (olive drab)		colour = { 162, 70, 215 },		colour = { 107, 142, 0 },		colour = { 107, 183, 0 },
		colour = { 129, 129, 0 },		
		tint = 0,
		badge="Wolfhead.tga"
	},
	{
		--Allied Player 2 (yellow)		colour = { 39, 166, 0 },
	colour = { 255, 190, 0 },
		tint = 0,
		badge="Wolfhead.tga"
	},
	{
		--Allied Player 3 (green)		colour = { 186, 78, 255 },
		colour = { 65, 135, 44 },
		tint = 0,
		badge="Wolfhead.tga"
	},
	{
		--Allied Player 4 (teal)		colour = { 12, 200, 175 },		colour = { 24, 169, 150 },
		colour = { 25, 202, 222 },
		tint = 0,
		badge="Wolfhead.tga"
	},
	{
		--Enemy Player 1 (red)		colour = { 227, 6, 0 },
		colour = { 216, 6, 0 },
		tint = 0,
		badge="Wolfhead.tga"
	},
	{
		--Enemy Player 2 (orange)		colour = { 254, 108, 0 },		colour = { 233, 122, 36 },
		colour = { 235, 125, 40 },
		tint = 0,
		badge="Wolfhead.tga"
	},
	{
		--Enemy Player 3 (burgundy)
		colour = { 162, 54, 107 },
		tint = 0,
		badge="Wolfhead.tga"
	},
	{
		--Enemy Player 4 (brown)		colour = { 163, 92, 15},
		colour = { 112, 71, 0 },
		tint = 0,
		badge="Wolfhead.tga"
	}
}


