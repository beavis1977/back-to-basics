import("util/demand.ai")

strategy_command_tree = {}

--------------------------------------------------------------------------------

function strategy_command_tree:init()
	self.chosenTreeIdx = nil
	self.prefIndexChosen = nil
end

function GetCommandTreeIndexFromUPGID(upgradePBG)
	if upgradePBG==nil then return -1 end
	local treeCount = CommandTree_Count(s_selfplayer)
	local pbgid = BP_GetID(upgradePBG)
	for ti=0,treeCount-1 do
		local ctUpgradePBG = CommandTree_GetMainUpgrade(s_selfplayer, ti)
		if BP_GetID(ctUpgradePBG) == pbgid then
			return ti
		end
	end
	return -1 -- error
end

function strategy_command_tree:choose_initial_tree()
	local treeCount = CommandTree_Count(s_selfplayer)

	-- do this even with zero - pts (would put this in init, but PBG_GetCount isn't updated yet)
	for ti=0,treeCount-1 do
		local upgradePBG = CommandTree_GetMainUpgrade(s_selfplayer, ti)
		if PBG_GetCount(s_selfplayer, upgradePBG) > 0 then
			return ti, upgradePBG
		end
	end

	local upgradePBG = nil
	local newChosenTreeIdx = nil
	local prefPath = nil

	-- get the personality we chose at the beginning
	if s_personality.command_tree_pref_tbl and s_personality.command_tree_pref_tbl.main then
		upgradePBG = s_personality.command_tree_pref_tbl.main
		local treeIndex = GetCommandTreeIndexFromUPGID(upgradePBG)
		if treeIndex ~= -1 then
			newChosenTreeIdx = treeIndex
			prefPath = AI_RandMax(#s_personality.command_tree_pref_tbl.choices)+1
		end
	end

	if newChosenTreeIdx==nil then
		newChosenTreeIdx = AI_RandMax(treeCount-1)
		upgradePBG = CommandTree_GetMainUpgrade(s_selfplayer, newChosenTreeIdx)
	end

	if upgradePBG~= nil and AIProduction_CanProduceNow(s_selfplayer, PITEM_PlayerUpgrade, upgradePBG, DEFAULT_HERO_ID) then
		Request_Production(s_selfplayer, TGROUP_CommandTree, TPRIORITY_CommandTree, PITEM_PlayerUpgrade, upgradePBG, DEFAULT_HERO_ID)
		-- this is our chosen tree
		return newChosenTreeIdx, upgradePBG, prefPath
	end

	--print("ERROR could not find command tree!!!")
end

function strategy_command_tree:do_purchase()
	--CustomDebug.Clear()
	--CustomDebug.Print("[Strategy tree purchases]")

	if self.chosenTreeIdx == nil then
		--CustomDebug.Print("  Initial tree choice...")
		local chosenIndex, upgradePBG, prefPath = self:choose_initial_tree()
		if chosenIndex ~= nil then
			--CustomDebug.Print("    Chosen tree ID: "..chosenIndex)
			--CustomDebug.Print("    Pref Path ID: "..prefPath)
			self.chosenTreeIdx = chosenIndex
			self.prefIndexChosen = prefPath
			s_player_ability_list = GetPlayerAbilityTable(s_current_race, upgradePBG)
		end
	end

	-- check to see if we have a commandpoint to spend
	local commandResource = AIResource_GetCurrentResources(s_selfplayer).command
	if commandResource == 0 then
		--CustomDebug.Print("  No CPs, early exit.")
		return
	end

	-- if we are following a pref-tree then lets try it
	if self.prefIndexChosen then
		--CustomDebug.Print("  Processing pref tree...")
		local pref_tbl = s_personality.command_tree_pref_tbl.choices[self.prefIndexChosen]
		if pref_tbl then
			local count = #pref_tbl
			local prefcount = 0
			-- run through the choice table in order, making sure we have each
			for i=1,count do
				-- count the total number of these items, including those that have been requested but not active
				local command_item = pref_tbl[i]
				local upg_count = UtilPBG_CountTotal(command_item)
				if upg_count <= 0 then
					if AIProduction_CanProduceNow(s_selfplayer, PITEM_PlayerUpgrade, command_item, DEFAULT_HERO_ID) then
						prefcount = prefcount + 1
						Request_Production(s_selfplayer, TGROUP_CommandTree, TPRIORITY_CommandTree, PITEM_PlayerUpgrade, command_item, DEFAULT_HERO_ID)
						--CustomDebug.Print("    Produce:"..BP_GetName(command_item))
					--else
						--CustomDebug.Print("    Can't produce:"..BP_GetName(command_item))
					end
					--aipoptrace()
					return
				else
					prefcount = prefcount + 1
				end
			end
			if commandResource >= 5 then
				self.prefIndexChosen = nil

				--[[CustomDebug.Print('ERROR: Command tree path reached 5 CP - invalid tech route! Path index: '..self.prefIndexChosen..', path upgrades:')
				for i=1,count do
					local command_item = pref_tbl[i]
					CustomDebug.Print('  '..BP_GetName(command_item))
				end]]
			elseif prefcount >= count then
				--CustomDebug.Print("    >>> Done with all pref choices")
				self.prefIndexChosen = nil
			else
				return
			end
		else
			--CustomDebug.Print('    Command tree path error')
			self.prefIndexChosen = nil
		end
	end

	if self.chosenTreeIdx ~= nil then
		local count = CommandTree_GetItemCount(s_selfplayer, self.chosenTreeIdx)
		for i=0,count do
			local command_item = CommandTree_GetItemAt(s_selfplayer, self.chosenTreeIdx, i)
			if command_item ~= nil and AIProduction_CanProduceNow(s_selfplayer, PITEM_PlayerUpgrade, command_item, DEFAULT_HERO_ID) then
				Request_Production(s_selfplayer, TGROUP_CommandTree, TPRIORITY_CommandTree, PITEM_PlayerUpgrade, command_item, DEFAULT_HERO_ID)
				break
			end
		end
	end
end
