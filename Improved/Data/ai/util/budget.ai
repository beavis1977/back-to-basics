-----------------------------
-- AI Budget System
--

local max = math.max
local min = math.min
local floor = math.floor

ai_budget = {}

function ai_budget:init()
	self.budget_list = {}
end

-- weight 0 means always run
function ai_budget:add(budget_group, task_group, purchase_func, w)
	self.budget_list[budget_group] = {
		taskGroup = task_group,
		weight = w,
		purchaseFunc = purchase_func,
	}
end

function ai_budget:remove(budget_group)
	Task_KillInactiveTasksInGroup(s_selfplayer, self.budget_list[budget_group].taskGroup)
	self.budget_list[budget_group] = nil
end

function ai_budget:setweight(budget_group, w)
	self.budget_list[budget_group].weight = w
end

function ai_budget:getweight(budget_group)
	return self.budget_list[budget_group].weight
end

function ai_budget:do_purchases(zero_weight_budgets)
	local resInActiveTasks = Task_CountCost(s_selfplayer, false)
	local availableResources = ResourceAmount_Subtract(AIResource_GetCurrentResources(s_selfplayer), resInActiveTasks)

	if zero_weight_budgets then
		for i,v in pairs(self.budget_list) do
			if v.weight == 0 then
				local resInactive = Task_CountGroupCost(s_selfplayer, v.taskGroup, false)
				Task_KillInactiveTasksInGroup(s_selfplayer, v.taskGroup)
				availableResources = ResourceAmount_Add(availableResources, resInactive)

				local did_purchase, used_resources = v.purchaseFunc(availableResources, 1)
				if used_resources then
					availableResources = ResourceAmount_ClampToZero(ResourceAmount_Subtract(availableResources, used_resources))
				end
			end
		end
	else
		--CustomDebug.Clear()
		--CustomDebug.Print("~~Budget~~")
		--CustomDebug.Print("  ResInTasks: "..res_to_str(resInActiveTasks))

		local totalWeight = 0
		local budget_choices = {}
		for budget_group,budget in pairs(self.budget_list) do
			if budget.weight > 0 then
				totalWeight = totalWeight + budget.weight
				budget_choices[budget_group] = true
			end
		end

		local totalWeightAtStartDiv = 1 / max(1, totalWeight)

		while totalWeight * totalWeightAtStartDiv > 0.6 do
			local weightRand = AI_RandRange(0, floor(totalWeight))
			local curWeight = 0
			for budget_group,v in pairs(budget_choices) do
				local budget = self.budget_list[budget_group]
				curWeight = curWeight + budget.weight
				if curWeight >= weightRand then
					--CustomDebug.Print("  ~ "..budget_group)

					local resInactive = Task_CountGroupCost(s_selfplayer, budget.taskGroup, false)
					Task_KillInactiveTasksInGroup(s_selfplayer, budget.taskGroup)
					availableResources = ResourceAmount_Add(availableResources, resInactive)

					local did_purchase, used_resources = budget.purchaseFunc(availableResources, 1)
					if used_resources then
						if not did_purchase then
							used_resources = ResourceAmount_Mult(used_resources, min(1, budget.weight * totalWeightAtStartDiv * 3.0))
						end
						availableResources = ResourceAmount_ClampToZero(ResourceAmount_Subtract(availableResources, used_resources))
						--CustomDebug.Print("    -Used Resources: "..res_to_str(used_resources).." Res Left: "..res_to_str(availableResources))
					end

					totalWeight = totalWeight - budget.weight
					budget_choices[budget_group] = nil

					break
				end
			end
			if availableResources.manpower < 450 then
				break
			end
		end
	end
end
