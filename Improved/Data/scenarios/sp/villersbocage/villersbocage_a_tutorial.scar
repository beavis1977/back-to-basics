function Tutorial_Init()

	sg_TutorialTank = SGroup_CreateIfNotFound("sg_TutorialTank")
	sg_TutorialTarget = SGroup_CreateIfNotFound("sg_TutorialTarget")
	g_APAbilityComplete = false
	
	t_Tutorial = {
		upgradeHPID 	= false,
		commandPoints	= 0, 
		
		bCCTriggered	= false,
		CCFlashId		= false,
		CCButtonId		= false,
		
		bHealTriggered	= false,
		HealButtonId	= false,
		HealFlashId		= false,
		repairHPId		= false,
		
		bHEvsTank		= false,
		HEvsTankHPId	= false,
		HEvsTankFlashId	= false,
		
		tTanks			= {
			SBP.CW.CROMWELL, 
			SBP.CW.STAGHOUND, 
			SBP.CW.FIREFLY,
			SBP.CW.STUART,
		},
		tInf			= {
			SBP.CW.TOMMIES,
			SBP.CW.SAPPER, 
			SBP.CW.LIEUTENANT,
		},
	}
	
	t_Tutorial.t_Upgrades = {
		{
			upg 		= UPG.SP.VILLERS_BOCAGE.LOADER1,
			flashID		= false,
		},
		{	
			upg 		= UPG.SP.VILLERS_BOCAGE.RADIO_OP1,
			flashID		= false,
		},
		{
			upg 		= UPG.SP.VILLERS_BOCAGE.DRIVER1,
			flashID		= false,
		},
		{
			upg 		= UPG.SP.VILLERS_BOCAGE.GUNNER1,
			flashID		= false,
		},
	}
	
	t_Tutorial.t_Speech = {
		repairs = {
			{ACTOR.VILLERS_BOCAGE.Voss,  6031725},
			{ACTOR.VILLERS_BOCAGE.Voss,  6031727},
			{ACTOR.VILLERS_BOCAGE.Voss,  6031728},
		},
		
		APRounds = {
			{ACTOR.VILLERS_BOCAGE.Voss,  6010600},
			{ACTOR.VILLERS_BOCAGE.Voss,  6010610},
		}
		
	}
	
	
	Rule_AddInterval(Tutorial_Rule_Tutorial, 1)
	

end

Scar_AddInit(Tutorial_Init)

function Tutorial_Rule_Tutorial()


	--********************** TUTORIAL: Buy an Upgrade ************************************
	-- tutorial to highlight the commander tree button when the player receives a command
	-- point.
	--************************************************************************************
	if t_Tutorial.bCCTriggered == false
	and Player_GetResource(player1, RT_Command) >= 1 then
		t_Tutorial.CCFlashId = UI_FlashBinding("company_commander", true, BT_UI_Weak_CommanderBtn)
		t_Tutorial.CCButtonId = HintPoint_AddToTaskbarBinding("company_commander", 6019507, true)
		Rule_AddOneShot(Tutorial_Remove_CommandPointHints, 30)
		t_Tutorial.bCCTriggered = true
	end
	
	--********************** TUTORIAL: Heal Tank ****************************************
	-- this button highlight will appear when the player has need of repairing his tank.
	--***********************************************************************************
	if t_Tutorial.bHealTriggered == false
	and Rule_Exists(CVB_RuleBreakdown) == false
	and Rule_Exists(DC_RuleBreakdown) == false
	and SGroup_GetAvgHealth(sg_Tiger) <= 0.5 
	and Misc_IsSGroupSelected(sg_Tiger, ANY) == true then
		t_Tutorial.HealButtonId = HintPoint_AddToAbilityButton( ABILITY.SP.VILLERS_BOCAGE.TIGER_REPAIR, 6019519, true)
		t_Tutorial.HealFlashId = UI_FlashAbilityButton( ABILITY.SP.VILLERS_BOCAGE.TIGER_REPAIR, true, BT_UI_Weak_AbilityBtn )
		
		Util_AutoIntel( t_Tutorial.t_Speech.repairs, true)
		Rule_AddOneShot( Tutorial_RemoveHealHighlight, 30)
		if Rule_Exists(Tutorial_CheckTankRepair) == false then
			Rule_AddInterval(Tutorial_CheckTankRepair, 1)
		end
		t_Tutorial.bHealTriggered = true
	end
	
	--********************** TUTORIAL: HE vs. Tank ****************************************
	-- This hint will appear when the player is up against a tank and is using HE rounds, when 
	-- he should be using AP Rounds.
	--***********************************************************************************
	if t_Tutorial.bHEvsTank == false
	and SGroup_IsEmpty( sg_Tiger) == false
	and Misc_IsSGroupSelected( sg_Tiger, ANY) == true
	and SGroup_IsUnderAttack( sg_Tiger, ANY, 10) == true
	and SGroup_GetNumSlotItem( sg_Tiger, SLOT_ITEM.VILLERS_BOCAGE.SP.TIGER_AMMO, ANY ) > 0 then
		
		SGroup_GetLastAttacker( sg_Tiger, sg_temp)
		if SGroup_ContainsBlueprints( sg_temp, t_Tutorial.tTanks, ANY ) == true
		and SGroup_ContainsBlueprints( sg_temp, t_Tutorial.tInf, ANY ) == false then
			
			Util_AutoIntel( t_Tutorial.t_Speech.APRounds)
			Rule_AddOneShot( Tutorial_RemoveHEvsTankHighlight, 30)
			t_Tutorial.HEvsTankHPId = HintPoint_Add( SGroup_GetRandomSpawnedSquad( sg_temp ), true, 6019508)
			t_Tutorial.HEvsTankButtonHPId = HintPoint_AddToAbilityButton( ABILITY.SP.VILLERS_BOCAGE.TOGGLE_AMMO_AP, 144500, true)
			t_Tutorial.HEvsTankFlashId = UI_FlashAbilityButton( ABILITY.SP.VILLERS_BOCAGE.TOGGLE_AMMO_AP, true, BT_UI_Weak_AbilityBtn)
			t_Tutorial.bHEvsTank = true
			
		end
	end

end

function Tutorial_Remove_CommandPointHints()
	
	UI_StopFlashing( t_Tutorial.CCFlashId )
	HintPoint_Remove( t_Tutorial.CCButtonId )

end

function Tutorial_CheckTankRepair()

	if SGroup_IsDoingAbility(sg_Tiger, ABILITY.SP.VILLERS_BOCAGE.TIGER_REPAIR, ANY) == true then
		if t_Tutorial.repairHPId == false then
			t_Tutorial.repairHPId = HintPoint_Add(sg_Tiger, true, 6019520)
		end
	elseif t_Tutorial.repairHPId ~= false then
		HintPoint_Remove(t_Tutorial.repairHPId)
		Rule_RemoveMe()
	end

end

function Tutorial_RemoveHealHighlight()

	UI_StopFlashing( t_Tutorial.HealFlashId )
	HintPoint_Remove( t_Tutorial.HealButtonId )

end

function Tutorial_RemoveHEvsTankHighlight()

	UI_StopFlashing( t_Tutorial.HEvsTankFlashId )
	HintPoint_Remove( t_Tutorial.HEvsTankHPId )
	HintPoint_Remove( t_Tutorial.HEvsTankButtonHPId )

end
