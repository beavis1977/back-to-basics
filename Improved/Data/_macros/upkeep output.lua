
-- This macro loads ebps from squads and outputs manpower and fuel upkeep per minute

local globalModifier = 0.6

function pLoadRgd(path)
	local loaded, rgd = pcall(loadRgd, path)
	if loaded and rgd then
		return rgd
	elseif rgd then
		print("Error: Cannot load RGD "..path.."\n  - Stack trace:\n"..rgd) return nil
	else
		print("Error: Not permitted to load RGD "..path) return nil
	end
end

local m = 60*8*globalModifier

function each_file(rgd)
	if rgd.GameData.squad_loadout_ext then
		local sqSize = 0
		local upkeepManpower = 0
		local upkeepFuel = 0
		for i,v in pairs(rgd.GameData.squad_loadout_ext.unit_list) do
			if i ~= '$REF' and v.type.type ~= '' then
				sqSize = sqSize + v.num
				local e = pLoadRgd("attrib\\attrib\\"..string.sub(v.type.type, 0, string.len(v.type.type)-3).."rgd")
				if e and e.GameData.cost_ext then
					upkeepManpower = upkeepManpower + e.GameData.cost_ext.upkeep.manpower * v.num
					upkeepFuel = upkeepFuel + e.GameData.cost_ext.upkeep.fuel * v.num
				end
			end
		end
		if sqSize > 0 then
			print(string.format("%s (%d): %.2f / %.2f", rgd.name, sqSize, upkeepManpower*m, upkeepFuel*m))
		end
	end
end

function at_end()
end
