function each_file(rgd)
	local w = rgd.GameData.weapon_bag
	if not w then return end
	
	local r = w.range
	
	print(string.format("%s", rgd.name))
	
	print(string.format("\tmax: %d => %d", r.max, math.floor(r.max*0.9)))
	r.max = math.floor(r.max*0.9)
	
	print(string.format("\tdistant: %d => %d", r.mid.distant, r.mid.distant*0.9))
	r.mid.distant = math.floor(r.mid.distant*0.9)
	
	print(string.format("\tlong: %d => %d", r.mid.long, r.mid.long*0.9))
	r.mid.long = math.floor(r.mid.long*0.9)
	
	if r.mid.medium/0.92 > r.mid.long then
		print(string.format("\tmedium: %d => %d", r.mid.medium, math.min(r.mid.long, r.mid.medium*0.9)))
		r.mid.medium = math.min(r.mid.long, math.floor(r.mid.medium*0.9))
	end
	
	if r.mid.short/0.92 > r.mid.medium then
		print(string.format("\tshort: %d => %d", r.mid.medium, math.min(r.mid.medium, r.mid.short*0.9)))
		r.mid.short = math.min(r.mid.medium, math.floor(r.mid.short*0.9))
	end
	
	--rgd:save()
end

function at_end()
end
