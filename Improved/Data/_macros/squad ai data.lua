
local function pLoadRgd(path)
	local loaded, rgd = pcall(loadRgd, path)
	if loaded and rgd then
		return rgd
	elseif rgd then
		print("Error: Cannot load RGD "..path.."\n  - Stack trace:\n"..rgd) return nil
	else
		print("Error: Not permitted to load RGD "..path) return nil
	end
end

print("local AI_SquadData = {")

function each_file(rgd)
	local anticlass = {0,0,0,0,0}
	
	print(string.format("\t[\"%q\"] = {", rgd.path))
	
	-- Process loadout information
	if rgd.GameData.squad_loadout_ext then
		for i,v in pairs(rgd.GameData.squad_loadout_ext.unit_list) do
			if i ~= "$REF" v.max > 0 and v.type ~= "" then
				local e = pLoadRgd("attrib\\attrib\\"..string.sub(v.type.type, 0, -3).."rgd")
				
				
				
			end
		end
	end
	
	-- Consider applied upgrades
	if rgd.GameData.squad_upgrade_apply_ext then
		for i,v in pairs(rgd.GameData.squad_upgrade_apply_ext.upgrades) do
			if i ~= "$REF" then
				if v ~= "" then
				
				end
			end
		end
	end
end

function at_end()
	print("}")
end
