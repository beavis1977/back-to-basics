
local eps = 0.0001

local settings = {
	
	["Small Arms"] = {
		["weapons"] = {
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\la_fiere_m1917_browning_30_cal_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m1917_browning_30_cal_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m1917_browning_30_cal_hmg_checkpoint.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_cckw.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_jeep_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_m3_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_turret_mount.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_turret_mount_hellcat.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_turret_mount_staghound.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\mg_hq50cal.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\mgnest_50cal.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\mgnest_50cal_sp.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\la_fiere_baker_infantry_m1918_browning_automatic_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\la_fiere_baker_vehicle_m1918_browning_automatic_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1918_browning_automatic_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_coaxial_vehicle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_coaxial_vehicle_hellcat.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_lmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_lmg_checkpoint.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_vehicle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_leader_m1_thompson_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_unit_2_m1_thompson_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_unit_3_m1_thompson_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_baker_unit_1_m1_thompson_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m1_thompson_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m1_thompson_smg_engineer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m3_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m3_smg_weapon_team.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\pistol\\colt_m1911_45_pistol.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\la_fiere_m1_carbine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_carbine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_garand_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_garand_rifle_wc.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\paratrooper_m1_garand_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\ranger_m1_garand_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\squad_a_unit_1_m1_carbine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\squad_a_unit_5_m1_garand_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\squad_b_leader_m1_carbine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\squad_b_unit_5_m1_garand_rifle.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_2xhmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_bren.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_hmg_emplacement.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_hmg_mobile_sp.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg_carrier.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg_pintle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_besa_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_besa_hull_kangaroo.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_coaxial_avre.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_coaxial_vehicle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_coaxial_vehicle_command_tank.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_firefly_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg_sapper.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg_silencer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\pistol\\cw_webley_revolver.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_elite.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_highlander.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_recon.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_short_range.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_weapon_crew.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\_mg42_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg_emp.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg_nest.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg_sp_m01.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg_sp_m01_attackplayer.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg_sp_m01_autotarget.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg_sp_m01_shingle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg_sp_m01_trenchmg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg_sp_m03.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg_sp_suppression_only.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_mgnest.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_mgnest_building_upgrade.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\sp_gen_mg42_hmg_no_suppression.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_coaxial_generic.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_hull_jt.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_hull_king_tiger.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_lmg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_motorcycle_sidecar.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_turret_mounted.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle_geschutzwagen.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle_rear.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\villers_bocage_mg42_coaxial_generic.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\villers_bocage_mg42_coaxial_generic_main.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\villers_bocage_mg42_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg_sp_m01_noautotarget.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg_sturmpioneer.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg_weapon_team.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp44_assault_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\pioneer_mp40_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\trun_mp44_assault_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\pistol\\luger_p08_9mm_pistol.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\pistol\\villers_bocage_luger_p08_9mm_pistol.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_leader.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_luftwaffe.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_vg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_wc.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_z2.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_zf.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\obergrenadier_kar_98k_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stg44_assault_rifle_semi_auto.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stormtrooper_kar_98k_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stormtrooper_stg44_assault_rifle_semi_auto.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\heavy_machine_gun\\pe_mgnest_50cal.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_250_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_hetzer.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_221.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_222.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_250_9.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\pe_m1919a4_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\sub_machine_gun\\fg_42_assault_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\sub_machine_gun\\pe_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\pistol\\pe_pistol.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_noscope.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\pe_kar_98k_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\pe_kar_98k_rifle_leader.rgd"] = true,

			["attrib\\attrib\\weapon\\boys_at_rifle.rgd"] = true,
		},
		["values"] = {
			['tp_heavy'] = {
				accuracy_multiplier = 0.82,
				damage_multiplier = 0.82,
				penetration_multiplier = 1,
				suppression_multiplier = 0.4,
			},
			['tp_light'] = {
				accuracy_multiplier = 0.9,
				damage_multiplier = 0.9,
				penetration_multiplier = 1,
				suppression_multiplier = 0.5,
			},
			['tp_garrison_cover'] = {
				accuracy_multiplier = 0.75,
				damage_multiplier = 0.75,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_garrison_halftrack'] = {
				accuracy_multiplier = 0.7,
				damage_multiplier = 0.7,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_trench'] = {
				accuracy_multiplier = 0.75,
				damage_multiplier = 0.75,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
		},
	},
	
	['Sniper Rifles'] = {
		['weapons'] = {
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\la_fiere_craft_m1903a4_sniper_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1903a4_sniper_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1903a4_springfield.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1903a4_springfield_vital_shot.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\commonwealth_sharpshooter_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_sharpshooter.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_sharpshooter_recon.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\lee_enfield_scoped_commando.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\lee_enfield_sniper_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\lee_enfield_sniper_rifle_commando.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\gewehr_43.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\gewehr_43_la_fiere.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\gewehr_43_sniper_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\gewehr_43_sp_m06.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_noscope_vital_shot.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_sharpshooter.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\pe_sniper_rifle.rgd"] = true,
		},
		["values"] = {
			['tp_heavy'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 1,
			},
			['tp_light'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 1,
			},
			['tp_garrison_cover'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_garrison_halftrack'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_trench'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
		},
	},
	
	['Ballistic weapons'] = {
		['weapons'] = {
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\anti_tank_gun\\la_fiere_m1_57mm_atg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\anti_tank_gun\\m1_57mm_atg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\anti_tank_gun\\m1_57mm_atg_ap_shell.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\anti_tank_gun\\m3_37mm_atg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\anti_tank_gun\\m5_76mm_atg.rgd"] = true,

			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\76mm_hellcat_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\76mm_hellcat_gun_tankwars.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\hellcat_tankwars.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m1897a4_75mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m1a1c_76mm_sherman_upgunned.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m3_75mm_m4_sherman_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m3_75mm_m8_scott_howitzer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m3_90mm_M36_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m3_90mm_pershing_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m3_90mm_pershing_hvap.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m4_105mm_sherman_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m6_37mm_greyhound_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m6_37mm_greyhound_gun_staghound.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m6_37mm_greyhound_gun_staghound_stun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m7_3in_m10_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m8_hmc_gun.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\anti_tank_gun\\cw_17pounder_at.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_17_pounder_achilles_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_17_pounder_firefly_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_2_pounder_tetrarch_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_avre_churchill_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_close_support_tetrarch_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_m6_37mm_staghound_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_m6_37mm_stuart_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_oqf_75mm_cromwell_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_qf_2_pdr_crusader_mk2_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_qf_6_pdr_churchill_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_qf_6_pdr_crusader_mk3_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\m3_75mm_m4a4_sherman_gun.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\50mm_pak_38.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\75mm_pak_40.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\75mm_pak_40_geschutzwagen.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\flak36_88mm_atg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\flak36_88mm_atg_sp_caen.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\flak36_88mm_atg_sp_carpiquet.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\flak36_88mm_atg_sp_m01.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\trun_flak36_88mm_atg_v0.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\trun_flak36_88mm_atg_v1.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\128mm_jagdtiger_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\37mm_hotchkiss_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk36_88mm_tiger_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk36_88mm_tiger_gun_df.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk36_88mm_tiger_gun_longrange.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk36_88mm_tiger_gun_spg_ace.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk37_pziv_short_barrel.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\flak_43_37mm_aa_ostwind_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk39_50mm_puma_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk40_75mm_pziv_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk42_75mm_panther_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk42_75mm_pantherturm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\stuh42_105mm_haubitze_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\stuh42_105mm_stug_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\stuk40_75mm_stug_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\villers_bocage_kwk36_88mm_tiger_gun_he.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\villers_bocage_kwk36_88mm_tiger_gun_he_double.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\villers_bocage_kwk36_88mm_tiger_gun_pzgr.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\villers_bocage_kwk36_88mm_tiger_gun_pzgr_double.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\villers_bocage_kwk36_88mm_tiger_gun_regular.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\anti_tank_gun\\37mm_pak_35_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\anti_tank_gun\\75mm_pak_40_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\anti_tank_gun\\75mm_pak_40_marderiii.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\anti_tank_gun\\apcr_37mm_pak_35_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\anti_tank_gun\\apcr_75mm_pak_40_marderiii.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\apcr_pak39_hetzer_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\apcr_pak43_88mm_jagdpanther_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\apcr_pak_43_88mm_hummel.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\apcr_pe_kwk40_75mm_pziv_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\apcr_pe_kwk42_75mm_panther_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\kwk39_50mm_h35_upgun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak39_hetzer_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak43_88mm_jagdpanther_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak43_88mm_kingtiger_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak43_88mm_kingtiger_gun_campaign.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak43_88mm_kingtiger_gun_longrange.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak_43_88mm_hummel.rgd"] = true,
		},
		["values"] = {
			['tp_heavy'] = {
				accuracy_multiplier = 0.55,
				damage_multiplier = 0.75,
				penetration_multiplier = 1,
				suppression_multiplier = 1,
			},
			['tp_light'] = {
				accuracy_multiplier = 0.65,
				damage_multiplier = 0.85,
				penetration_multiplier = 1,
				suppression_multiplier = 1,
			},
			['tp_garrison_cover'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 0.75,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_garrison_halftrack'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 0.75,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_trench'] = {
				accuracy_multiplier = 0.35,
				damage_multiplier = 0.6,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
		},
	},
	
	['Light AOE'] = {
		['weapons'] = {
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\la_fiere_stun_mkii_a1_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\mine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\mine_improved.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\mkii_a1_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\mkii_a1_grenade_building.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\satchel_charge_throw.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\satchel_charge_throw_airborne.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\satchel_charge_throw_sp_m01.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\villers_bocage_satchel_charge_throw.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\grenade\\commonwealth_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\grenade\\commonwealth_grenade_commando.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\grenade\\bundled_at_stielgranate.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\grenade\\bundled_stielgranate.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\grenade\\la_fiere_bundled_stielgranate.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\grenade\\stielhandgranate24_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\grenade\\stielhandgranate24_grenade_stun.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\air_dropped_mine.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\booby_trap_weapon.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\booby_trap_weapon_point.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\incendiary_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\incendiary_grenade_fallschirmjager.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\pe_anti_tank_grenade.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\teller_mine.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\trun_teller_mine.rgd"] = true,
		},
		["values"] = {
			['tp_heavy'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 1,
			},
			['tp_light'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 1,
			},
			['tp_garrison_cover'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_garrison_halftrack'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_trench'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
		},
	},
	
	['Mortars'] = {
		['weapons'] = {
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\mortar\\m2_60mm_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\mortar\\m2_60mm_mortar_airburst_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\mortar\\m2_60mm_mortar_barrage.rgd"] = true,

			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmap_allies_mortars.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmap_axis_mortars.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\commonwealth_mortar_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\commonwealth_mortar_barrage_victor_target.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\commonwealth_mortar_counter_battery.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\commonwealth_mortar_creeping_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\commonwealth_mortar_supercharge.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\cw_3inch_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\cw_3inch_mortar_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\cw_3inch_mortar_emplacement.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\cw_3inch_mortar_supercharge.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\villers_bocage_cw_3inch_mortar.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar_barrage_emplacement.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar_barrage_pe.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar_bunker_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar_emplacement.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_grw34_81mm_mortar_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_grw34_81mm_mortar_barrage_instant.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_quick_grw34_81mm_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_quick_grw34_81mm_mortar_m01.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_quick_grw34_81mm_mortar_m01_npc.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_smoke_grw34_81mm_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_smoke_grw34_81mm_mortar_instant.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\mortar\\incendiary_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\mortar\\pe_grw34_81mm_mortar_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\mortar\\pe_mortar.rgd"] = true,
		},
		["values"] = {
			['tp_heavy'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 0.5,
			},
			['tp_light'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 0.75,
			},
			['tp_garrison_cover'] = {
				accuracy_multiplier = 2,
				damage_multiplier = 1.5,
				penetration_multiplier = 2,
				suppression_multiplier = 0,
			},
			['tp_garrison_halftrack'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_trench'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 0.75,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
		},
	},
	
	['Heavy AOE'] = {
		['weapons'] = {
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\demolitions.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\demolitions_commando.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\howitzer\\m1a1_75mm_gun_mobile.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\howitzer\\m2a1_105mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\howitzer\\m2a1_105mm_gun_air_detonation.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\howitzer\\m2a1_105mm_gun_anti_tank.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\howitzer\\la_fiere_axis_m2a1_105mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\rockets\\calliope.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m1897a4_75mm_gun_he_high_angle.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_40mm_bofors_aa_gun.rgd"] = true,

			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\150mm_rockets.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\280mm_rockets.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\320mm_rockets.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\e3_offmapartillery_force_only.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_altsound_sp.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_always_deform.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_always_deform_plane.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_always_deform_sp_m13.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_axis_defensive.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_axis_defensive_airburst.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_axis_defensive_burn.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_axis_defensive_rockets.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_foo_first_round.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_long_tom.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_railway.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_sector.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_sp_m04.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_villers_bocage.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapbombardement.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\sp_offmapartillery_always_deform_m01.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\v1_rocket.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_105mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_105mm_gun_overwatch.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_105mm_gun_supercharge.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_105mm_gun_victor_target.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_counter_battery.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_creeping_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_dummy.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_groundtarget.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_overwatch.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_super_charge.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_victor_target.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\us_105mm_gun.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\barrage_weapon\\commonwealth_offmapartillery.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\barrage_weapon\\commonwealth_offmapartillery_114mm.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\barrage_weapon\\commonwealth_offmapartillery_creeping_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\barrage_weapon\\offmap_3_inch_rockets.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\howitzer\\flak36_88mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\howitzer\\lefh18_105mm_gun.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\150mm_nebelwerfer.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\150mm_nebelwerfer_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\150mm_nebelwerfer_battery.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\firestorm_rocket.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\stuka.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\stuka_donkey.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\stuka_incendiary.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\stuka_pe_hotchkiss.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\stuka_pe_hotchkiss_incendiary.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\howitzer\\150mm_sfh_18_l30.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\howitzer\\150mm_sIG_33.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\howitzer\\pe_artillery.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\rockets\\pe_rocket.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\barrage_weapon\\pe_offmapartillery.rgd"] = true,

			["attrib\\attrib\\weapon\\goliath_weapon.rgd"] = true,
		},
		["values"] = {
			['tp_heavy'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 0.5,
				penetration_multiplier = 1,
				suppression_multiplier = 0.5,
			},
			['tp_light'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 0.75,
				penetration_multiplier = 1,
				suppression_multiplier = 0.7,
			},
			['tp_garrison_cover'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 0.5,
				suppression_multiplier = 0,
			},
			['tp_garrison_halftrack'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_trench'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 0.33,
				penetration_multiplier = 0.75,
				suppression_multiplier = 0,
			},
		},
	},
	
	['Infantry AT'] = {
		['weapons'] = {
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\baker_squad_m18_recoilless_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\jeep_m18_recoilless_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\la_fiere_fire_recoiless.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\m18_recoilless_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\m18_recoilless_rifle_veteran_tactics.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\m1_bazooka_atw.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\m1_bazooka_atw_veteran_tactics.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\infantry_anti_tank_weapon\\piat.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\infantry_anti_tank_weapon\\villers_bocage_piat.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\panzerschreck_atw.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\panzerschreck_atw_pe.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\panzerschreck_eselshreck.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\panzerschreck_eselshreck_01.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\trun_panzerschreck_atw.rgd"] = true,
		},
		["values"] = {
			['tp_heavy'] = {
				accuracy_multiplier = 0.65,
				damage_multiplier = 0.75,
				penetration_multiplier = 1,
				suppression_multiplier = 1,
			},
			['tp_light'] = {
				accuracy_multiplier = 0.75,
				damage_multiplier = 0.85,
				penetration_multiplier = 1,
				suppression_multiplier = 1,
			},
			['tp_garrison_cover'] = {
				accuracy_multiplier = 0.5,
				damage_multiplier = 0.75,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_garrison_halftrack'] = {
				accuracy_multiplier = 0.45,
				damage_multiplier = 0.75,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_trench'] = {
				accuracy_multiplier = 0.4,
				damage_multiplier = 0.6,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
		},
	},
	
	['BumBum Wagons'] = {
		['weapons'] = {
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\quad_50_cal_m2hb.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_20mm_crusader_aa.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\flak38_20mm_light_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\flak38_20mm_light_gun_m02.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\flak38_20mm_light_gun_sp_m07.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\flak38_wirblewind.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk38_20mm_light_armoured_car_gun.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\flak38_20mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\kwk38_20mm_sdkfz_222.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\kwk38_20mm_sdkfz_250_9.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pe_flak38_20mm_light_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pe_flak38_20mm_light_gun_caen_sp.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pe_flak38_20mm_luftwaffe.rgd"] = true,
		},
		["values"] = {
			['tp_heavy'] = {
				accuracy_multiplier = 0.65,
				damage_multiplier = 0.75,
				penetration_multiplier = 1,
				suppression_multiplier = 0.5,
			},
			['tp_light'] = {
				accuracy_multiplier = 0.75,
				damage_multiplier = 0.85,
				penetration_multiplier = 1,
				suppression_multiplier = 0.7,
			},
			['tp_garrison_cover'] = {
				accuracy_multiplier = 0.6,
				damage_multiplier = 0.65,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_garrison_halftrack'] = {
				accuracy_multiplier = 0.6,
				damage_multiplier = 0.65,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_trench'] = {
				accuracy_multiplier = 0.35,
				damage_multiplier = 0.6,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
		},
	},
	
	['Flamethrowers'] = {
		['weapons'] = {
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\churchill_crocodile_flame_projector.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\churchill_crocodile_flame_projector_hull_down.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\flame_test_projectile.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\flammenwerfer_42_backpack.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\m2_backpack_flamethrower.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\m2_backpack_flamethrower_sp_m01.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\sherman_crocodile_flame_projector.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\trun_flammenwerfer_42_backpack.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\flame_throwers\\commonwealth_flamethrower.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\flame_throwers\\commonwealth_wasp_flamethrower.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\flame_throwers\\halftrack_flammenwerfer.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\flame_throwers\\halftrack_flammenwerfer_right.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\flame_throwers\\pe_flamethrower.rgd"] = true,
		},
		["values"] = {
			['tp_heavy'] = {
				accuracy_multiplier = 1.5,
				damage_multiplier = 1.5,
				penetration_multiplier = 1,
				suppression_multiplier = 1,
			},
			['tp_light'] = {
				accuracy_multiplier = 1.25,
				damage_multiplier = 1.25,
				penetration_multiplier = 1,
				suppression_multiplier = 1,
			},
			['tp_garrison_cover'] = {
				accuracy_multiplier = 1.5,
				damage_multiplier = 1.5,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_garrison_halftrack'] = {
				accuracy_multiplier = 1,
				damage_multiplier = 1.5,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
			['tp_trench'] = {
				accuracy_multiplier = 1.5,
				damage_multiplier = 1.5,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
		},
	},
}

function each_file(rgd)
	if rgd.GameData.weapon_bag then
		local wb = rgd.GameData.weapon_bag
		
		for s_i,s in pairs(settings) do
			if s.weapons[rgd.path] then
				print(rgd.path..' ('..s_i..')')
				
				local changed = false
				
				for cover_type,fields in pairs(s.values) do
					print("\t["..cover_type.."]:")
					
					local cover_table = wb.cover_table[cover_type]
					if not cover_table then
						print("\t\tWARNING: This cover type doesn't exist for this weapon!")
					else
						for field,value in pairs(fields) do
							if not cover_table[field] then
								print("\t\t[\""..field.."\"]:")
								print("\t\t\tWARNING: This cover table doesn't exist for this cover table!")
							else
								if math.abs(cover_table[field] - value) > eps then
									print("\t\t[\""..field.."\"]:")
									print("\t\t\t"..string.format("%.2f => %.2f", cover_table[field], value))
									cover_table[field] = value
									changed = true
								end
							end
						end
					end
				end
				
				if changed then
					--rgd:save()
				end
				
				return
			end
		end
		
		--print(rgd.path..' ~ SETTING NOT FOUND')
	end
end

function at_end()
	print("Done!")
end
