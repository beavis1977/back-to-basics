

local weaponsOfInterest = {
	-- Core
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m1917_browning_30_cal_hmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1918_browning_automatic_rifle.rgd"] = 2,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m1_thompson_smg.rgd"] = 4,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m1_thompson_smg_engineer.rgd"] = 3,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m3_smg.rgd"] = 3,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_carbine.rgd"] = 5,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_garand_rifle.rgd"] = 6,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\paratrooper_m1_garand_rifle.rgd"] = 6,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1903a4_springfield.rgd"] = 2,
	
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_hmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg_carrier.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg.rgd"] = 4,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg_sapper.rgd"] = 4,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg_silencer.rgd"] = 5,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield.rgd"] = 5,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_recon.rgd"] = 3,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_elite.rgd"] = 4,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_highlander.rgd"] = 6,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_weapon_crew.rgd"] = 2,
	["attrib\\attrib\\weapon\\boys_at_rifle.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_20mm_crusader_aa.rgd"] = 1,
	
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_lmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg_sturmpioneer.rgd"] = 4,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp44_assault_rifle.rgd"] = 4,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\pioneer_mp40_smg.rgd"] = 3,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle.rgd"] = 4,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_vg.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\gewehr_43.rgd"] = 2,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stg44_assault_rifle_semi_auto.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stormtrooper_stg44_assault_rifle_semi_auto.rgd"] = 4,
	
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\sub_machine_gun\\fg_42_assault_rifle.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43.rgd"] = 4,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_noscope.rgd"] = 3,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_noscope_obergrenadier.rgd"] = 5,
	
	-- Other
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m1917_browning_30_cal_hmg_checkpoint.rgd"] = 1,
	["attrib\\attrib\\weapon\\plane_weapons\\m2hb_hmg_p47_thunderbolt_50_cal_force_fire.rgd"] = 2,
	["attrib\\attrib\\weapon\\plane_weapons\\vickers_hmg_hawker_typhoon_303_cal_force_fire.rgd"] = 2,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_hmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_jeep_hmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_turret_mount.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_m3_halftrack.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\quad_50_cal_m2hb.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_vehicle.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_coaxial_vehicle.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_hull.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_lmg_checkpoint.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_garand_rifle_wc.rgd"] = 2,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\ranger_m1_garand_rifle.rgd"] = 4,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1903a4_sniper_rifle.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\pistol\\colt_m1911_45_pistol.rgd"] = 1,
	
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_hmg_emplacement.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_bren.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg_pintle.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_besa_hull.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_firefly_hull.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_sharpshooter.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_sharpshooter_recon.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\lee_enfield_sniper_rifle_commando.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\lee_enfield_scoped_commando.rgd"] = 2,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\pistol\\cw_webley_revolver.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_40mm_bofors_aa_gun.rgd"] = 1,
	
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_mgnest.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_hull.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_motorcycle_sidecar.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_turret_mounted.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle_geschutzwagen.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle_rear.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_coaxial_generic.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg_weapon_team.rgd"] = 2,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_wc.rgd"] = 3,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\obergrenadier_kar_98k_rifle.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stormtrooper_kar_98k_rifle.rgd"] = 4,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\gewehr_43_sniper_rifle.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\pistol\\luger_p08_9mm_pistol.rgd"] = 1,
	
	["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pe_flak38_20mm_luftwaffe.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\kwk38_20mm_sdkfz_222.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\kwk38_20mm_sdkfz_250_9.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\flak38_wirblewind.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk38_20mm_light_armoured_car_gun.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\flak_43_37mm_aa_ostwind_gun.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_250_halftrack.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_hetzer.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_221.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_222.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_250_9.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_noscope_vital_shot.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\pe_kar_98k_rifle.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_luftwaffe.rgd"] = 3,
	
	-- Flamethrowers
	["attrib\\attrib\\weapon\\allies\\flame_throwers\\m2_backpack_flamethrower.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\flame_throwers\\commonwealth_wasp_flamethrower.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\flame_throwers\\flammenwerfer_42_backpack.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\flame_throwers\\churchill_crocodile_flame_projector.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\flame_throwers\\sherman_crocodile_flame_projector.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\flame_throwers\\halftrack_flammenwerfer.rgd"] = 1,
}

local function p(str, flag)
	if flag then
		print(str)
	end
end

function getDPSData(rgd)
	local weapon = rgd.GameData.weapon_bag
	
	local numShots = 1+(weapon.reload.frequency.max+weapon.reload.frequency.min)/2
	local reloadDuration = (weapon.reload.duration.max+weapon.reload.duration.min)/2
	local fireAimTime = (weapon.aim.fire_aim_time.min+weapon.aim.fire_aim_time.max)/2
	local readyAimTime = (weapon.aim.ready_aim_time.min+weapon.aim.ready_aim_time.max)/2
	local cooldown = (weapon.cooldown.duration.max+weapon.cooldown.duration.min)/2
	local burstDuration = 0
	local clip = numShots
	
	if weapon.burst.can_burst == true then
		burstDuration = (weapon.burst.duration.max+weapon.burst.duration.min)/2
		clip = numShots * (weapon.burst.rate_of_fire.max+weapon.burst.rate_of_fire.min)/2 * burstDuration
	end
	
	local damagePerSequence = (weapon.damage.max + weapon.damage.min) * 0.5 * clip
	
	function getRangeDPS(range)
		local damageDuration = numShots * (
				weapon.cooldown.duration_multiplier[range]*cooldown
				+ fireAimTime * weapon.aim.fire_aim_time_multiplier[range]
				+ weapon.fire.wind_up
				+ weapon.fire.wind_down
				+ burstDuration
			)
		local fullSequenceTime = damageDuration
			+ reloadDuration * weapon.reload.duration_multiplier[range]
			+ readyAimTime
		
		if fullSequenceTime == 0 then return {0, 0, 0} end
		
		return {damagePerSequence / fullSequenceTime, damageDuration, fullSequenceTime}
	end
	return getRangeDPS("short"), clip, damagePerSequence
end

function each_file(rgd)
	if rgd.GameData and rgd.GameData.weapon_bag then
		local printFlag = weaponsOfInterest[rgd.path] ~= nil
		
		local weapon = rgd.GameData.weapon_bag
		
		p(rgd.name..":", printFlag)
		p(string.format("  Range: %d - %d", weapon.range.min, weapon.range.max), printFlag)
		if weapon.range.max < weapon.range.mid.distant then
			p("  !! Max range lower than distant range ("..weapon.range.mid.distant..")", printFlag)
		end
		local damageBreakpoints = string.format("%d - %d", weapon.damage.min, weapon.damage.max)
		for i=2,5 do
			damageBreakpoints = damageBreakpoints .. ", " .. string.format("%d - %d", weapon.damage.min*i, weapon.damage.max*i)
		end
		p(string.format("  Damage: %s", damageBreakpoints), printFlag)
		
		local avgDamage = (weapon.damage.max + weapon.damage.min) * 0.5
		local rangeDPS, clip, damagePerSequence = getDPSData(rgd, avgDamage)
		
		p(string.format("  Clip: %.1f", clip), printFlag)
		p(string.format("  DamagePerSequence: %d-%d", weapon.damage.min*clip, weapon.damage.max*clip), printFlag)
		
		local newAccuracy = tonumber(string.format("%.2f", math.max(weapon.accuracy.medium, weapon.accuracy.short*0.9)))
		
		local dps, damageDuration, fullSequenceTime = rangeDPS[1], rangeDPS[2], rangeDPS[3]
		local hitrate1 = clip/fullSequenceTime*weapon.accuracy.short*60
		local hitrate2 = clip/fullSequenceTime*newAccuracy*60
		
		local dpsStr1 = string.format("%5.2f :: %3.1fRPM", dps*weapon.accuracy.short, hitrate1)
		local dpsStr2 = string.format("%5.2f :: %3.1fRPM", dps*newAccuracy, hitrate2)
		
		p(string.format("    | (%2d / %3.2f => %3.2f) :: %s => %s",
			weapon.range.mid.short, weapon.accuracy.short, newAccuracy, dpsStr1, dpsStr2),
			printFlag)
			
		p("", printFlag)
		
		weapon.accuracy.short = newAccuracy
		--rgd:save()
	end
end

function at_end()

end
