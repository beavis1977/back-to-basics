local showWeaponsOfInterestOnly = true -- Set this to true to only output weapons that are part of the weaponsOfInterest table
local showTTKInfo = false
local outputCommonLoadouts = true

local commonHealthValues = { -- [name] = {is allied, squad member count, entity health, received accuracy}
	["Rifles"] = {true, 6, 85, 1},
	--["Ranger"] = {true, 6, 90, 0.85},
	["AB"] = {true, 6, 85, 0.95},
	["Pathf"] = {true, 4, 110, 0.85},
	["Armored"] = {true, 5, 90, 0.85},
	--["Eng"] = {true, 3, 90, 1},
	["Tommy"] = {true, 5, 95, 1},
	--["Sapper"] = {true, 4, 100, 1},
	["Commando"] = {true, 6, 90, 0.85},
	["Volks"] = {false, 5, 92, 1},
	["Gren"] = {false, 4, 115, 1},
	--["Storms"] = {false, 4, 115, 0.85},
	["KCH"] = {false, 4, 120, 0.85},
	--["Obers"] = {false, 4, 95, 0.85},
	["Pgren"] = {false, 5, 95, 1},
	["Falls"] = {false, 5, 93, 0.95},
	--["Pzpio"] = {false, 3, 110, 1},
	--["Ketten"] = {false, 1, 400, 0.75},
	--["Jeep"] = {true, 1, 400, 0.9},
	--["Bike"] = {false, 1, 400, 0.9},
	--["BC"] = {true, 1, 400, 1},
}
local globalAccuracyMult = 0.965
local globalDamageMult = 1.0

local weaponsOfInterest = {
	-- Core
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m1917_browning_30_cal_hmg.rgd"] = 1,
	--["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1918_browning_automatic_rifle.rgd"] = 2,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1918a2_browning_automatic_rifle.rgd"] = 2,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1941_johnson_lmg.rgd"] = 2,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m1_thompson_smg.rgd"] = 4,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m1_thompson_smg_engineer.rgd"] = 3,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m3_smg.rgd"] = 3,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_carbine.rgd"] = 5,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_garand_rifle.rgd"] = 6,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\paratrooper_m1_garand_rifle.rgd"] = 6,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1903a4_springfield.rgd"] = 2,

	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_hmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg_carrier.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg.rgd"] = 4,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg_sapper.rgd"] = 4,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg_silencer.rgd"] = 6,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield.rgd"] = 5,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_recon.rgd"] = 3,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_elite.rgd"] = 2,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_highlander.rgd"] = 6,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_weapon_crew.rgd"] = 2,
	["attrib\\attrib\\weapon\\boys_at_rifle.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_20mm_crusader_aa.rgd"] = 1,

	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_lmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg_sturmpioneer.rgd"] = 4,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp44_assault_rifle.rgd"] = 4,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp44_assault_rifle_assault_grenadier.rgd"] = 4,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\pioneer_mp40_smg.rgd"] = 3,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle.rgd"] = 4,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_vg.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\gewehr_43.rgd"] = 2,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stg44_assault_rifle_semi_auto.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stormtrooper_stg44_assault_rifle_semi_auto.rgd"] = 4,

	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\sub_machine_gun\\fg_42_assault_rifle.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43.rgd"] = 4,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_noscope.rgd"] = 3,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_noscope_obergrenadier.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\panzerfusilier_kar_98k_rifle.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\pe_kar_98k_rifle.rgd"] = 3,

	-- Other
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m1917_browning_30_cal_hmg_checkpoint.rgd"] = 1,
	["attrib\\attrib\\weapon\\plane_weapons\\m2hb_hmg_p47_thunderbolt_50_cal_force_fire.rgd"] = 2,
	["attrib\\attrib\\weapon\\plane_weapons\\vickers_hmg_hawker_typhoon_303_cal_force_fire.rgd"] = 2,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_hmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_jeep_hmg.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_turret_mount.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_m3_halftrack.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\quad_50_cal_m2hb.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_vehicle.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_coaxial_vehicle.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_hull.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_lmg_checkpoint.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_garand_rifle_wc.rgd"] = 2,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\ranger_m1_garand_rifle.rgd"] = 4,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\paratrooper_m1_carbine.rgd"] = 5,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1903a4_sniper_rifle.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\pistol\\colt_m1911_45_pistol.rgd"] = 1,

	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_hmg_emplacement.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_bren.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg_pintle.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_besa_hull.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_firefly_hull.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_sharpshooter.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_sharpshooter_recon.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\lee_enfield_sniper_rifle_commando.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\lee_enfield_scoped_commando.rgd"] = 2,
	["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\pistol\\cw_webley_revolver.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_40mm_bofors_aa_gun.rgd"] = 1,

	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_mgnest.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_mgnest_building_upgrade.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_hull.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_motorcycle_sidecar.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_turret_mounted.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle_geschutzwagen.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle_rear.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_coaxial_generic.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg_weapon_team.rgd"] = 2,
	["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg_tiger_crew.rgd"] = 2,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_wc.rgd"] = 3,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\obergrenadier_kar_98k_rifle.rgd"] = 5,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stormtrooper_kar_98k_rifle.rgd"] = 4,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\gewehr_43_sniper_rifle.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\pistol\\luger_p08_9mm_pistol.rgd"] = 1,

	["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pe_flak38_20mm_luftwaffe.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\kwk38_20mm_sdkfz_222.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\kwk38_20mm_sdkfz_250_9.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\flak38_wirblewind.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk38_20mm_light_armoured_car_gun.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\flak_43_37mm_aa_ostwind_gun.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_250_halftrack.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_hetzer.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_221.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_222.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_250_9.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_noscope_vital_shot.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_luftwaffe.rgd"] = 3,
	["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_sharpshooter.rgd"] = 1,

	-- Flamethrowers
	["attrib\\attrib\\weapon\\allies\\flame_throwers\\m2_backpack_flamethrower.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies_cw\\flame_throwers\\commonwealth_wasp_flamethrower.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\flame_throwers\\flammenwerfer_42_backpack.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\flame_throwers\\churchill_crocodile_flame_projector.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\flame_throwers\\sherman_crocodile_flame_projector.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\flame_throwers\\sherman_crocodile_flame_projector_toggle.rgd"] = 1,
	["attrib\\attrib\\weapon\\axis\\flame_throwers\\halftrack_flammenwerfer.rgd"] = 1,
	
	-- SP
	--Able Squad
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_leader_m1_thompson_smg.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_unit_2_m1_thompson_smg.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_unit_3_m1_thompson_smg.rgd"] = 1,
	
	-- Baker Squad
	["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\squad_b_leader_m1_carbine.rgd"] = 1,
	["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\la_fiere_baker_infantry_m1918_browning_automatic_rifle.rgd"] = 1,
}

local commonWeaponLoadouts = { -- is allied, weapons list {rgd path, count}
	["BAR Riflemen"] = {true, {
			{"attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_garand_rifle.rgd", 4},
			{"attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1918a2_browning_automatic_rifle.rgd", 2},
	}},
	["Tommy BoysAT"] = {true, {
		{"attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_garand_rifle.rgd", 3},
		{"attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1918_browning_automatic_rifle.rgd", 2},
	}},
	["Sappers Bren"] = {true, {
		{"attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg_sapper.rgd", 3},
		{"attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg.rgd", 1},
	}},
	["Gren LMG42"] = {false, {
		{"attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle.rgd", 3},
		{"attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_lmg.rgd", 1},
	}},
	["Schutzen"] = {false, {
		{"attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43.rgd", 2},
		{"attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_lmg.rgd", 1},
	}},
	["PGren LMG42"] = {false, {
		{"attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stg44_assault_rifle_semi_auto.rgd", 4},
		{"attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_lmg.rgd", 1},
	}},
	["Jagers"] = {false, {
		{"attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43.rgd", 3},
		{"attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_sharpshooter.rgd", 1},
	}},
	["Recon"] = {true, {
		{"attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_recon.rgd", 2},
		{"attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_sharpshooter_recon.rgd", 1},
	}},
	["Able"] = {true, {
		{"attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_leader_m1_thompson_smg.rgd", 1},
		{"attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_unit_2_m1_thompson_smg.rgd", 1},
		{"attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_unit_3_m1_thompson_smg.rgd", 2},
	}},
	["Baker"] = {true, {
		{"attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\squad_b_leader_m1_carbine.rgd", 1},
		{"attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\la_fiere_baker_infantry_m1918_browning_automatic_rifle.rgd", 2},
	}},
}

local ranges = {"distant", "long", "medium", "short"}

local function getHTK(hp, dmg)
	return math.ceil(hp / dmg)
end

local function getTTKStr(hp, baseTTK, avgDamage, cover_table)
	return string.format("[ Open: %3ds Light: %3ds Heavy: %3ds ]",
		baseTTK / cover_table.tp_open.accuracy_multiplier * getHTK(hp, avgDamage * cover_table.tp_open.damage_multiplier),
		baseTTK / cover_table.tp_light.accuracy_multiplier * getHTK(hp, avgDamage * cover_table.tp_light.damage_multiplier),
		baseTTK / cover_table.tp_heavy.accuracy_multiplier * getHTK(hp, avgDamage * cover_table.tp_heavy.damage_multiplier))
end

function getDPSData(rgd)
	local weapon = rgd.GameData.weapon_bag

	local numShots = 1+(weapon.reload.frequency.max+weapon.reload.frequency.min)/2
	local reloadDuration = (weapon.reload.duration.max+weapon.reload.duration.min)/2
	local fireAimTime = (weapon.aim.fire_aim_time.min+weapon.aim.fire_aim_time.max)/2
	local readyAimTime = (weapon.aim.ready_aim_time.min+weapon.aim.ready_aim_time.max)/2
	local cooldown = (weapon.cooldown.duration.max+weapon.cooldown.duration.min)/2
	local burstDuration = 0
	local clip = numShots

	if weapon.burst.can_burst == true then
		burstDuration = (weapon.burst.duration.max+weapon.burst.duration.min)/2
		clip = numShots * (weapon.burst.rate_of_fire.max+weapon.burst.rate_of_fire.min)/2 * burstDuration
	end

	local damagePerSequence = (math.floor(weapon.damage.max * globalDamageMult) + math.floor(weapon.damage.min * globalDamageMult)) * 0.5 * clip

	function getRangeDPS(range)
		local damageDuration = numShots * (
				weapon.fire.wind_up
				+ weapon.fire.wind_down
				+ burstDuration
			)
			+ (numShots - 1) * (
				weapon.cooldown.duration_multiplier[range]*cooldown
				+ fireAimTime * weapon.aim.fire_aim_time_multiplier[range]
			)
		local fullSequenceTime = damageDuration
			+ reloadDuration * weapon.reload.duration_multiplier[range]
			+ readyAimTime

		if fullSequenceTime == 0 then return {0, 0, 0} end

		return {damagePerSequence * weapon.accuracy[range] * globalAccuracyMult / fullSequenceTime, damageDuration, fullSequenceTime}
	end
	return {getRangeDPS(ranges[1]), getRangeDPS(ranges[2]), getRangeDPS(ranges[3]), getRangeDPS(ranges[4])}, clip, damagePerSequence
end

print("# WEAPON DPS #")
print("#   Values in order: DPS (DPS of weapons in squad) :: Hitrate (Hitrate of weapons in squad)")
print("")

function each_file(rgd)
	if (not showWeaponsOfInterestOnly or weaponsOfInterest[rgd.path] ~= nil) and rgd.GameData and rgd.GameData.weapon_bag then
		local weapon = rgd.GameData.weapon_bag
		local minDmg = math.floor(weapon.damage.min * globalDamageMult)
		local maxDmg = math.floor(weapon.damage.max * globalDamageMult)

		print(rgd.name..":")
		print(string.format("  Range: %d - %d", weapon.range.min, weapon.range.max))
		if weapon.range.max < weapon.range.mid.distant then
			print("  !! Max range lower than distant range ("..weapon.range.mid.distant..")")
		end
		local damageBreakpoints = string.format("%d - %d", minDmg, maxDmg)
		for i=2,5 do
			damageBreakpoints = damageBreakpoints..", "..string.format("%d - %d", minDmg*i, maxDmg*i)
		end
		print(string.format("  Damage: %s", damageBreakpoints))

		local weaponsInSquad = 1
		if weaponsOfInterest[rgd.path] then
			weaponsInSquad = weaponsOfInterest[rgd.path]
			print(string.format("  WeaponsInSquad: %d", weaponsInSquad))
		end

		local avgDamage = (minDmg + maxDmg) * 0.5
		local rangeDPS, clip, damagePerSequence = getDPSData(rgd, avgDamage)

		print(string.format("  Clip: %.1f", clip))
		print(string.format("  DamagePerSequence: %d-%d", math.floor(weapon.damage.min * globalDamageMult)*clip, math.floor(weapon.damage.max * globalDamageMult)*clip))

		if showTTKInfo then
			print("  HitsToKill:")
			for healthValuesName,healthValuesInfo in pairs(commonHealthValues) do
				if healthValuesInfo[1] == (rgd.path:find("weapon\\allies", 0, true) == nil) then
					local openDMG = {
						maxDmg*weapon.cover_table.tp_open.damage_multiplier,
						avgDamage*weapon.cover_table.tp_open.damage_multiplier,
						minDmg*weapon.cover_table.tp_open.damage_multiplier,
					}
					local lightDMG = {
						maxDmg*weapon.cover_table.tp_light.damage_multiplier,
						avgDamage*weapon.cover_table.tp_light.damage_multiplier,
						minDmg*weapon.cover_table.tp_light.damage_multiplier,
					}
					local heavyDMG = {
						maxDmg*weapon.cover_table.tp_heavy.damage_multiplier,
						avgDamage*weapon.cover_table.tp_heavy.damage_multiplier,
						minDmg*weapon.cover_table.tp_heavy.damage_multiplier,
					}
					print(string.format("    |      vs %7s (%3d * %d): Open %2d-%2d-%2d Light %2d-%2d-%2d Heavy %2d-%2d-%2d",
						healthValuesName, healthValuesInfo[3], healthValuesInfo[2],
						getHTK(healthValuesInfo[3], openDMG[1]), getHTK(healthValuesInfo[3], openDMG[2]), getHTK(healthValuesInfo[3], openDMG[3]),
						getHTK(healthValuesInfo[3], lightDMG[1]), getHTK(healthValuesInfo[3], lightDMG[2]), getHTK(healthValuesInfo[3], lightDMG[3]),
						getHTK(healthValuesInfo[3], heavyDMG[1]), getHTK(healthValuesInfo[3], heavyDMG[2]), getHTK(healthValuesInfo[3], heavyDMG[3])
					))
				end
			end
		end

		print("  DPS:")
		for range=1,4 do
			local dps, damageDuration, fullSequenceTime = rangeDPS[range][1], rangeDPS[range][2], rangeDPS[range][3]
			local hitrate = clip/fullSequenceTime*weapon.accuracy[ranges[range]]*globalAccuracyMult

			local dpsStr
			if weaponsInSquad > 1 then
				dpsStr = string.format("%5.2f (%6.2f) :: %3.1fRPM (%3.1fRPM)", dps, dps * weaponsInSquad, hitrate * 60, hitrate * 60 * weaponsInSquad)
			else
				dpsStr = string.format("%5.2f :: %3.1fRPM", dps, hitrate * 60)
			end
			print(string.format("    | %7s (%2d / %3.2f) :: %s",
				ranges[range], weapon.range.mid[ranges[range]], weapon.accuracy[ranges[range]] * globalAccuracyMult, dpsStr))

			if showTTKInfo then
				for healthValuesName,healthValuesInfo in pairs(commonHealthValues) do
					if healthValuesInfo[1] == (rgd.path:find("weapon\\allies", 0, true) == nil) then
						local baseTTK = healthValuesInfo[2] / hitrate / healthValuesInfo[4]
						local TTKStr
						if weaponsInSquad > 1 then
							TTKStr = string.format("%s (%s)", getTTKStr(healthValuesInfo[3], baseTTK, avgDamage, weapon.cover_table), getTTKStr(healthValuesInfo[3], baseTTK / weaponsInSquad, avgDamage, weapon.cover_table))
						else
							TTKStr = string.format("%s", getTTKStr(healthValuesInfo[3], baseTTK, avgDamage, weapon.cover_table))
						end
						print(string.format("    |      vs %7s (%3d * %d / %.3f): %s",
							healthValuesName, healthValuesInfo[3], healthValuesInfo[2], healthValuesInfo[4], TTKStr))
					end
				end
			end
		end

		print("")
	end
end

function pLoadRgd(path)
	local loaded, rgd = pcall(loadRgd, path)
	if loaded and rgd then
		return rgd
	elseif rgd then
		print("Error: Cannot load RGD "..path.."\n  - Stack trace:\n"..rgd) return nil
	else
		print("Error: Not permitted to load RGD "..path) return nil
	end
end

function at_end()
	if outputCommonLoadouts then
		print("# COMMON LOADOUTS #\n")
		for loadoutName,loadoutInfo in pairs(commonWeaponLoadouts) do
			local weapons = loadoutInfo[2]

			print(loadoutName..":")
			local rangeDPS = {0, 0, 0, 0}

			for i=1,#weapons do
				local rgd = pLoadRgd(weapons[i][1])

				local weaponRangeDPS = getDPSData(rgd)
				for range=1,4 do
					rangeDPS[range] = rangeDPS[range] + weaponRangeDPS[range][1] * weapons[i][2]
				end
			end

			print("  DPS:")
			for range=1,4 do
				print(string.format("    | %7s :: %5.2f", ranges[range], rangeDPS[range]))
				if showTTKInfo then
					for healthValuesName,healthValuesInfo in pairs(commonHealthValues) do
						if healthValuesInfo[1] ~= loadoutInfo[1] then
							local TTKStr
							if rangeDPS[range] > 0 then
								TTKStr = string.format("%3.1fs", healthValuesInfo[2]*healthValuesInfo[3]/healthValuesInfo[4]/rangeDPS[range])
							else
								TTKStr = "INF"
							end
							print(string.format("    |    vs %7s : %s", healthValuesName, TTKStr))
						end
					end
				end
			end
			print("")
		end
	end
end
