function each_file(rgd)
	if rgd.GameData.weapon_bag then
		local w = rgd.GameData.weapon_bag
		if w.target_table.tp_infantry_sniper.accuracy_multiplier > 1.4 and w.target_table.tp_infantry_sniper.accuracy_multiplier < 1.6 then
			print(rgd.name..":")
			print(string.format("  accuracy_multiplier: %.2f -> %.2f", w.target_table.tp_infantry_sniper.accuracy_multiplier, 1.7))
			w.target_table.tp_infantry_sniper.accuracy_multiplier = 1.7
			--rgd:save()
		end
	end
end

function at_end()
end
