local function pLoadRgd(path)
	local loaded, rgd = pcall(loadRgd, path)
	if loaded and rgd then
		return rgd
	elseif rgd then
		print("Error: Cannot load RGD "..path.."\n  - Stack trace:\n"..rgd) return nil
	else
		print("Error: Not permitted to load RGD "..path) return nil
	end
end

function each_file(rgd)
	local reinforce_ext = rgd.GameData.squad_reinforce_ext
	local loadout_ext = rgd.GameData.squad_loadout_ext
	if reinforce_ext and loadout_ext then
		
		local totalCost = {0,0,0}
		local totalTime = 0
		local totalCount = 0
		local reinforceCount = 0
		
		for i,v in pairs(loadout_ext.unit_list) do
			if v.num and v.type.type and v.num > 0 and v.type.type ~= "" then
			
				totalCount = totalCount + v.num
				
				if v.type['$REF'] ~= 'type_loadout\\sync_weapon.lua' then
					reinforceCount = reinforceCount + v.num
				end
				
				local e = pLoadRgd("attrib\\attrib\\"..string.sub(v.type.type, 0, -5)..".rgd")
				
				if e then
					if e.GameData.cost_ext then
						totalTime = totalTime + v.num * e.GameData.cost_ext.time_cost.time_seconds
						totalCost[1] = totalCost[1] + v.num * e.GameData.cost_ext.time_cost.cost.manpower
						totalCost[2] = totalCost[2] + v.num * e.GameData.cost_ext.time_cost.cost.munition
						totalCost[3] = totalCost[3] + v.num * e.GameData.cost_ext.time_cost.cost.fuel
					end
				else
					print("### ERROR LOADING EBPS FOR "..rgd.name)
				end
			end
		end
		
		if reinforceCount > 1 then
			print("# "..rgd.name)
			print("\ttotalTime: "..string.format("%.1f", totalTime))
			print("\ttotalCount: "..string.format("%d (%d)", totalCount, reinforceCount))
			print("\treinforceTimePercentage: "..string.format("%.3f", reinforce_ext.time_cost_percentage.time_percentage))
			print("\treinforceCostPercentage: "..string.format("%.3f", reinforce_ext.time_cost_percentage.cost_percentage))
			print("\treinforceTime: "..string.format("%d", totalTime*reinforce_ext.time_cost_percentage.time_percentage/totalCount))
			print("\treinforceCost: "..string.format("%.1f (total: %.1f)",
				totalCost[1]*reinforce_ext.time_cost_percentage.cost_percentage/totalCount,
				totalCost[1]*reinforce_ext.time_cost_percentage.cost_percentage/totalCount*(reinforceCount - 1)
				--totalCost[2]*reinforce_ext.time_cost_percentage.cost_percentage/totalCount,
				--totalCost[3]*reinforce_ext.time_cost_percentage.cost_percentage/totalCount
			))
			
			--[[
			totalTime * reinforceTimePercentage * x / totalCount - totalTime * reinforceTimePercentage / totalCount = 3
			totalTime * reinforceTimePercentage / totalCount ( x - 1 ) = 3
			x - 1 = 3 / totalTime / reinforceTimePercentage * totalCount
			x = 3 / totalTime / reinforceTimePercentage * totalCount + 1
			]]
			
			--reinforce_ext.time_cost_percentage.time_percentage = reinforce_ext.time_cost_percentage.time_percentage + 3 / totalTime * totalCount
			
			--print("  >new reinforceTimePercentage: "..reinforce_ext.time_cost_percentage.time_percentage)
			--print("  >new reinforceTime: "..string.format("%d", totalTime*reinforce_ext.time_cost_percentage.time_percentage/totalCount))
			
			--rgd:save()
		end
	end
end

function at_end()
end
