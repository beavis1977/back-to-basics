local target_types = {
	'tp_infantry',
	'tp_infantry_flamethrower_death',
	'tp_infantry_heroic'
}
function each_file(rgd)
	if rgd.GameData.weapon_bag then
		local w = rgd.GameData.weapon_bag
		for i,t in pairs(target_types) do
			local c = w.critical_table[t]
			if c then
				local damage_bound_found = false
				local damage_bound_index = nil
				for j,wce in pairs(c) do
					if j ~= '$REF' then
						for k,ct in pairs(wce) do
							if k == 'damage_bound' then
								damage_bound_found = true
								damage_bound_index = j
								break
							end
						end
						if damage_bound_found then
							break
						end
					end
				end
				if damage_bound_found then
					local main_hit_found = false
					local make_casualty_hit_found = false
					local main_hit_index = nil
					local make_casualty_hit_index = nil
					for j,ct in pairs(c[damage_bound_index]) do
						if j ~= '$REF' and j ~= 'damage_bound' then
							if ct.critical_type.critical == 'critical\\_no_critical.lua' then
								main_hit_found = true
								main_hit_index = j
								if make_casualty_hit_found then
									break
								end
							end
							if ct.critical_type.critical == 'critical\\make_casualty.lua' then
								make_casualty_hit_found = true
								make_casualty_hit_index = j
								if main_hit_found then
									break
								end
							end
						end
					end
					if main_hit_found and make_casualty_hit_found and c[damage_bound_index][make_casualty_hit_index].weight == 5 then
						print(rgd.name)
						print('  main hit index: '..main_hit_index)
						print('  make_casualty hit index: '..make_casualty_hit_index)
						print('  main hit weight: '..string.format('%.2f', c[damage_bound_index][main_hit_index].weight)..' -> '..string.format('%.2f', c[damage_bound_index][main_hit_index].weight + c[damage_bound_index][make_casualty_hit_index].weight))
						c[damage_bound_index][main_hit_index].weight = c[damage_bound_index][main_hit_index].weight + c[damage_bound_index][make_casualty_hit_index].weight
						c[damage_bound_index][make_casualty_hit_index].weight = 0
						c[damage_bound_index][make_casualty_hit_index].critical_type.critical = ''
						rgd:save()
					end
				end
			end
		end
	end
end

function at_end()
end
