
local settings = {

	["Ballistic weapons"] = {
		["weapons"] = {
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\anti_tank_gun\\la_fiere_m1_57mm_atg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\anti_tank_gun\\m1_57mm_atg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\anti_tank_gun\\m1_57mm_atg_ap_shell.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\anti_tank_gun\\m3_37mm_atg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\anti_tank_gun\\m5_76mm_atg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\baker_squad_m18_recoilless_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\jeep_m18_recoilless_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\la_fiere_fire_recoiless.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\m18_recoilless_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\m18_recoilless_rifle_veteran_tactics.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\m1_bazooka_atw.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\infantry_anti_tank_weapon\\m1_bazooka_atw_veteran_tactics.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\rockets\\calliope.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\76mm_hellcat_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\76mm_hellcat_gun_tankwars.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\hellcat_tankwars.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m1897a4_75mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m1a1c_76mm_sherman_upgunned.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m3_75mm_m4_sherman_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m3_75mm_m8_scott_howitzer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m3_90mm_M36_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m3_90mm_pershing_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m3_90mm_pershing_hvap.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m4_105mm_sherman_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m6_37mm_greyhound_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m6_37mm_greyhound_gun_staghound.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m7_3in_m10_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m8_hmc_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\anti_tank_gun\\cw_17pounder_at.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\infantry_anti_tank_weapon\\piat.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\infantry_anti_tank_weapon\\villers_bocage_piat.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_17_pounder_achilles_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_17_pounder_firefly_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_2_pounder_tetrarch_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_close_support_tetrarch_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_m6_37mm_staghound_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_m6_37mm_stuart_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_oqf_75mm_cromwell_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_qf_6_pdr_churchill_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\m3_75mm_m4a4_sherman_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\50mm_pak_38.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\75mm_pak_40.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\75mm_pak_40_geschutzwagen.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\flak36_88mm_atg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\flak36_88mm_atg_sp_caen.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\flak36_88mm_atg_sp_carpiquet.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\flak36_88mm_atg_sp_m01.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\trun_flak36_88mm_atg_v0.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\anti_tank_gun\\trun_flak36_88mm_atg_v1.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\grenade\\bundled_at_stielgranate.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\panzerfaust_atw.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\panzerfaust_atw_improved.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\panzerfaust_atw_sp_fireslowly.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\panzerschreck_atw.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\panzerschreck_atw_pe.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\panzerschreck_eselshreck.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\panzerschreck_eselshreck_01.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\trun_panzerschreck_atw.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\infantry_anti_tank_weapon\\villers_bocage_panzerfaust.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\128mm_jagdtiger_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\37mm_hotchkiss_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\flak_43_37mm_aa_ostwind_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk36_88mm_tiger_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk36_88mm_tiger_gun_df.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk36_88mm_tiger_gun_longrange.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk36_88mm_tiger_gun_spg_ace.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk37_pziv_short_barrel.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk39_50mm_puma_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk40_75mm_pziv_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk42_75mm_panther_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\kwk42_75mm_pantherturm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\stuh42_105mm_stug_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\stuk40_75mm_stug_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\villers_bocage_kwk36_88mm_tiger_gun_he.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\villers_bocage_kwk36_88mm_tiger_gun_he_double.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\villers_bocage_kwk36_88mm_tiger_gun_pzgr.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\villers_bocage_kwk36_88mm_tiger_gun_pzgr_double.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\villers_bocage_kwk36_88mm_tiger_gun_regular.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\anti_tank_gun\\37mm_pak_35_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\anti_tank_gun\\37mm_pak_35_halftrack_stielgranate_41.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\anti_tank_gun\\75mm_pak_40_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\anti_tank_gun\\75mm_pak_40_marderiii.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\anti_tank_gun\\apcr_37mm_pak_35_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\anti_tank_gun\\apcr_75mm_pak_40_marderiii.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\pe_anti_tank_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\infantry_anti_tank_weapon\\pe_infantry_anti_tank.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\rockets\\pe_rocket.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\apcr_pak39_hetzer_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\apcr_pak43_88mm_jagdpanther_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\apcr_pak_43_88mm_hummel.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\apcr_pe_kwk40_75mm_pziv_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\apcr_pe_kwk42_75mm_panther_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\kwk39_50mm_h35_upgun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak39_hetzer_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak43_88mm_jagdpanther_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak43_88mm_kingtiger_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak43_88mm_kingtiger_gun_campaign.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak43_88mm_kingtiger_gun_longrange.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak_40_henschel_aircraft.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak_40_henschel_aircraft_autofire.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\tank_gun\\pak_43_88mm_hummel.rgd"] = true,
			["attrib\\attrib\\weapon\\plane_weapons\\hispano_suiza_20mm_hawker_typhoon.rgd"] = true,
			["attrib\\attrib\\weapon\\plane_weapons\\hispano_suiza_20mm_hawker_typhoon_left.rgd"] = true,
			["attrib\\attrib\\weapon\\plane_weapons\\hispano_suiza_20mm_hawker_typhoon_right.rgd"] = true,
		},
		["criticals"] = {
			['tp_armour'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',				        100},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_crew_shock.lua',				40},
					{'critical\\vehicle_damage_tracks.lua',				30},
					{'critical\\vehicle_jam_maingun.lua',				20},
					{'critical\\vehicle_damage_engine.lua',				10},
				},
			},
			['tp_armour_elite'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',				        100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						25},
					{'critical\\vehicle_crew_shock.lua',				30},
					{'critical\\vehicle_damage_tracks.lua',				25},
					{'critical\\vehicle_damage_engine.lua',				10},
					{'critical\\vehicle_jam_maingun.lua',				10},
				},
			},
			['tp_armour_rear'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',				        40},
					{'critical\\vehicle_crew_shock.lua',				30},
					{'critical\\vehicle_damage_tracks.lua',				15},
					{'critical\\vehicle_damage_engine.lua',				15},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_damage_engine.lua',				50},
					{'critical\\vehicle_damage_tracks.lua',				20},
					{'critical\\vehicle_destroy_engine_rear.lua',		10},
					{'critical\\vehicle_lose_treads_or_wheels.lua',		10},
					{'critical\\vehicle_crew_shock.lua',				10},
				},
			},
			['tp_vehicle'] = {
				["damage_green"] = {
					{'critical\\vehicle_damage_engine.lua',				40},
					{'critical\\vehicle_damage_tracks.lua',				40},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_damage_engine.lua',				40},
					{'critical\\vehicle_damage_tracks.lua',				40},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
			},
			['tp_vehicle_halftrack'] = {
				["damage_green"] = {
					{'critical\\vehicle_damage_engine.lua',				40},
					{'critical\\vehicle_damage_tracks.lua',				40},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_damage_engine.lua',				40},
					{'critical\\vehicle_damage_tracks.lua',				40},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
			},
			['tp_vehicle_rear'] = {
				["damage_green"] = {
					{'critical\\vehicle_damage_engine.lua',				40},
					{'critical\\vehicle_damage_tracks.lua',				40},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_damage_engine.lua',				40},
					{'critical\\vehicle_damage_tracks.lua',				40},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
			}
		}
	},

	["Large AOE weapons"] = {
		["weapons"] = {
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\demolitions.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\demolitions_commando.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\satchel_charge_throw.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\satchel_charge_throw_airborne.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\satchel_charge_throw_sp_m01.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\villers_bocage_satchel_charge_throw.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\howitzer\\la_fiere_axis_m2a1_105mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\howitzer\\m1a1_75mm_gun_mobile.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\howitzer\\m2a1_105mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\howitzer\\m2a1_105mm_gun_air_detonation.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\howitzer\\m2a1_105mm_gun_anti_tank.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\tank_gun\\m1897a4_75mm_gun_he_high_angle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\150mm_rockets.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\280mm_rockets.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\320mm_rockets.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\e3_offmapartillery_force_only.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmap_allies_mortars.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_altsound_sp.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_always_deform.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_always_deform_plane.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_always_deform_sp_m13.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_axis_defensive.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_axis_defensive_airburst.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_axis_defensive_burn.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_axis_defensive_rockets.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_foo_first_round.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_railway.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_sector.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_sp_m04.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapartillery_villers_bocage.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmapbombardement.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\sp_offmapartillery_always_deform_m01.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\v1_rocket.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_105mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_105mm_gun_overwatch.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_105mm_gun_supercharge.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_105mm_gun_victor_target.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_counter_battery.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_creeping_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_dummy.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_groundtarget.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_overwatch.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_super_charge.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\cw_25_pounder_howitzer_victor_target.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\us_105mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\howitzer\\villers_bocage_defensive_tank_weapon.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\villers_bocage_cw_3inch_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\rocket\\rp3_rocket_l.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\rocket\\rp3_rocket_r.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_avre_churchill_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_avre_churchill_gun_anti_building.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\villers_bocage_dot_weapon.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\barrage_weapon\\commonwealth_offmapartillery.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\barrage_weapon\\commonwealth_offmapartillery_114mm.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\barrage_weapon\\commonwealth_offmapartillery_creeping_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\howitzer\\flak36_88mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\howitzer\\lefh18_105mm_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\villers_bocage_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\150mm_nebelwerfer.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\150mm_nebelwerfer_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\150mm_nebelwerfer_battery.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\firestorm_rocket.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\stuka.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\stuka_donkey.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\stuka_incendiary.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\stuka_pe_hotchkiss.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\rockets\\stuka_pe_hotchkiss_incendiary.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\stuh42_105mm_haubitze_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\howitzer\\150mm_sfh_18_l30.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\howitzer\\150mm_sIG_33.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\howitzer\\pe_artillery.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\barrage_weapon\\pe_offmapartillery.rgd"] = true,
			["attrib\\attrib\\weapon\\plane_weapons\\five_hundred_pound.rgd"] = true,
			["attrib\\attrib\\weapon\\plane_weapons\\plane_3_inch_rockets.rgd"] = true,
			["attrib\\attrib\\weapon\\plane_weapons\\plane_3_inch_rockets_searchfire.rgd"] = true,
			["attrib\\attrib\\weapon\\plane_weapons\\plane_5_inch_rockets.rgd"] = true,
			["attrib\\attrib\\weapon\\plane_weapons\\plane_5_inch_rockets_right.rgd"] = true,
			["attrib\\attrib\\weapon\\plane_weapons\\plane_henschel_rockets.rgd"] = true,
			["attrib\\attrib\\weapon\\plane_weapons\\stuka_bomb.rgd"] = true,
			["attrib\\attrib\\weapon\\goliath_weapon.rgd"] = true,
			["attrib\\attrib\\weapon\\plane_sml_crash.rgd"] = true,
			["attrib\\attrib\\weapon\\v1_launcher.rgd"] = true,
		},
		["criticals"] = {
			['tp_armour'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						80},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						20},
					{'critical\\vehicle_crew_shock.lua',				55},
					{'critical\\vehicle_damage_tracks.lua',				25},
				},
			},
			['tp_armour_elite'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						80},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						35},
					{'critical\\vehicle_crew_shock.lua',				50},
					{'critical\\vehicle_damage_tracks.lua',				15},
				},
			},
			['tp_armour_rear'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						80},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						20},
					{'critical\\vehicle_crew_shock.lua',				55},
					{'critical\\vehicle_damage_tracks.lua',				25},
				},
			},
			['tp_vehicle'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						80},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_crew_shock.lua',				40},
					{'critical\\vehicle_damage_engine.lua',				30},
					{'critical\\vehicle_damage_tracks.lua',				30},
				},
			},
			['tp_vehicle_halftrack'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						80},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_crew_shock.lua',				40},
					{'critical\\vehicle_damage_engine.lua',				30},
					{'critical\\vehicle_damage_tracks.lua',				30},
				},
			},
			['tp_vehicle_rear'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						80},
					{'critical\\vehicle_crew_shock.lua',				20},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_crew_shock.lua',				40},
					{'critical\\vehicle_damage_engine.lua',				30},
					{'critical\\vehicle_damage_tracks.lua',				30},
				},
			}
		}
	},

	["Light ballistic weapons"] = {
		["weapons"] = {
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\tank_gun\\cw_40mm_bofors_aa_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\tank_gun\\flak_43_37mm_aa_ostwind_gun.rgd"] = true,
			["attrib\\attrib\\weapon\\boys_at_rifle.rgd"] = true,
		},
		["criticals"] = {
			['tp_armour'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						30},
					{'critical\\vehicle_crew_shock.lua',				25},
					{'critical\\vehicle_damage_tracks.lua',				25},
					{'critical\\vehicle_damage_engine.lua',				10},
					{'critical\\vehicle_jam_maingun.lua',				10},
				},
			},
			['tp_armour_elite'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						65},
					{'critical\\vehicle_crew_shock.lua',				20},
					{'critical\\vehicle_damage_tracks.lua',				10},
					{'critical\\vehicle_damage_engine.lua',				5},
				},
			},
			['tp_armour_rear'] = {
				["damage_green"] = {
					{'critical\\_no_critical_rear.lua',					100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical_rear.lua',					30},
					{'critical\\vehicle_crew_shock.lua',				25},
					{'critical\\vehicle_damage_engine.lua',				25},
					{'critical\\vehicle_damage_tracks.lua',				20},
				},
			},
			['tp_vehicle'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						50},
					{'critical\\vehicle_damage_engine.lua',				20},
					{'critical\\vehicle_damage_tracks.lua',				20},
					{'critical\\vehicle_crew_shock.lua',				10},
				},
			},
			['tp_vehicle_halftrack'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						50},
					{'critical\\vehicle_damage_engine.lua',				20},
					{'critical\\vehicle_damage_tracks.lua',				20},
					{'critical\\vehicle_crew_shock.lua',				10},
				},
			},
			['tp_vehicle_rear'] = {
				["damage_green"] = {
					{'critical\\_no_critical_rear.lua',					100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical_rear.lua',					50},
					{'critical\\vehicle_damage_engine.lua',				20},
					{'critical\\vehicle_damage_tracks.lua',				20},
					{'critical\\vehicle_crew_shock.lua',				10},
				},
			}
		}
	},

	["Indirect ballistic weapons"] = {
		["weapons"] = {
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\mortar\\m2_60mm_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\mortar\\m2_60mm_mortar_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\barrage_weapon\\offmap_axis_mortars.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\cw_3inch_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\cw_3inch_mortar_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\cw_3inch_mortar_emplacement.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\mortar\\cw_3inch_mortar_supercharge.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar_barrage_emplacement.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar_barrage_pe.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar_bunker_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\grw34_81mm_mortar_emplacement.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_grw34_81mm_mortar_barrage.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_grw34_81mm_mortar_barrage_instant.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_quick_grw34_81mm_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_quick_grw34_81mm_mortar_m01.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\mortar\\sp_quick_grw34_81mm_mortar_m01_npc.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\air_dropped_mine.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\booby_trap_weapon.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\booby_trap_weapon_point.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\incendiary_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\incendiary_grenade_fallschirmjager.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\mortar\\incendiary_mortar.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\mortar\\pe_grw34_81mm_mortar_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\mortar\\pe_mortar.rgd"] = true,
		},
		["criticals"] = {
			['tp_armour'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						100},
				},
			},
			['tp_armour_elite'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						100},
				},
			},
			['tp_armour_rear'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',					    100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						100},
				},
			},
			['tp_vehicle'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						40},
					{'critical\\vehicle_crew_shock.lua',				30},
					{'critical\\vehicle_damage_engine.lua',				20},
					{'critical\\vehicle_damage_tracks.lua',				10},
				},
			},
			['tp_vehicle_halftrack'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',						100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						40},
					{'critical\\vehicle_crew_shock.lua',				30},
					{'critical\\vehicle_damage_engine.lua',				20},
					{'critical\\vehicle_damage_tracks.lua',				10},
				},
			},
			['tp_vehicle_rear'] = {
				["damage_green"] = {
					{'critical\\_no_critical.lua',					    100},
				},
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						40},
					{'critical\\vehicle_crew_shock.lua',				30},
					{'critical\\vehicle_damage_engine.lua',				20},
					{'critical\\vehicle_damage_tracks.lua',				10},
				},
			}
		}
	},

	["Guaranteed disable weapons"] = {
		["weapons"] = {
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\mine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\mine_improved.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\sticky_bomb.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\sticky_bomb_vet_2.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\teller_mine.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\trun_teller_mine.rgd"] = true,
		},
		["criticals"] = {
			['tp_armour'] = {
				["damage_green"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
			},
			['tp_armour_elite'] = {
				["damage_green"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
			},
			['tp_armour_rear'] = {
				["damage_green"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
			},
			['tp_vehicle'] = {
				["damage_green"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
			},
			['tp_vehicle_halftrack'] = {
				["damage_green"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
			},
			['tp_vehicle_rear'] = {
				["damage_green"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
				["damage_yellow"] = {
					{'critical\\vehicle_damage_tracks.lua',				50},
					{'critical\\vehicle_damage_engine.lua',				50},
				},
			},
		}
	},

	["Flamethrowers"] = {
		["weapons"] = {
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\churchill_crocodile_flame_projector.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\churchill_crocodile_flame_projector_hull_down.rgd"] = true,
			--["attrib\\attrib\\weapon\\allies\\flame_throwers\\flame_test_projectile.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\flammenwerfer_42_backpack.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\m2_backpack_flamethrower.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\flame_throwers\\trun_flammenwerfer_42_backpack.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\flame_throwers\\commonwealth_flamethrower.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\flame_throwers\\commonwealth_wasp_flamethrower.rgd"] = true,
		},
		["criticals"] = {
			['tp_infantry'] = {
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						100},
				}
			},
			['tp_infantry_heroic'] = {
				["damage_yellow"] = {
					{'critical\\_no_critical.lua',						100},
				}
			},
		}
	},

}

function updateCritical(t, index, critical)
	if t[index] then
		if t[index].critical_type['$REF'] ~= 'critical_type\\critical_single.lua' then
			print("\t\t\t[\""..index.."\"].critical_type['$REF'] = \"critical_type\\critical_single.lua\" (was: "..t[index].critical_type['$REF']..")")
			t[index].critical_type = {
				['$REF'] = "critical_type\\critical_single.lua",
				['critical'] = ""
			}
		end
		if t[index].critical_type.critical ~= critical[1] then
			print("\t\t\t[\""..index.."\"].critical_type.critical = "..string.format("%s", critical[1]).." (was: "..string.format("%s", tostring(t[index].critical_type.critical))..")")
			t[index].critical_type.critical = critical[1]
		end
		if t[index].weight ~= critical[2] then
			print("\t\t\t[\""..index.."\"].weight = "..critical[2].." (was: "..t[index].weight..")")
			t[index].weight = critical[2]
		end
	else
		print("\t\t\t[\""..index.."\"] = {}")
		print("\t\t\t[\""..index.."\"].critical_type.critical = "..string.format("%s", critical[1]))
		print("\t\t\t[\""..index.."\"].weight = "..critical[2])
		t[index] = {
			['$REF'] = 'tables\\weapon_critical_item.lua',
			['critical_type'] = {
				['$REF'] = 'critical_type\\critical_single.lua',
				['critical'] = critical[1],
			},
			['weight'] = critical[2],
		}
	end
end

function each_file(rgd)
	if rgd.GameData.weapon_bag then
		local wb = rgd.GameData.weapon_bag

		for s_i,s in pairs(settings) do
			if not s.weapons or s.weapons[rgd.path] then
				print(rgd.path..' ('..s_i..')')

				for unit_type,damage_bound_settings in pairs(s.criticals) do
					print("\t["..unit_type.."]:")

					local unit_type_criticals = wb.critical_table[unit_type]
					if not unit_type_criticals then
						print("\t\tWARNING: Unit type criticals don't exist for this weapon!")
					else

						for damage_bound,criticals in pairs(damage_bound_settings) do
							print("\t\t[\""..damage_bound.."\"]:")

							-- First, find the proper damage bound
							local damage_bound_criticals = nil
							for i,damage_bound_criticals_t in pairs(unit_type_criticals) do
								if i ~= '$REF' and damage_bound_criticals_t.damage_bound == "damage\\"..damage_bound..".lua" then
									damage_bound_criticals = damage_bound_criticals_t
									break
								end
							end
							if damage_bound_criticals == nil then
								print("\t\t\tERROR: Damage bound not found!")
							else
								-- Apply the changes, implement the defined criticals first, then remove or clear any extra
								-- criticals if there are too many (nulling values of entries of entries up to hit_05,
								-- removing entries that are higher index than that)

								for i=1,#criticals do
									local index = string.format("hit_%02d", i)
									updateCritical(damage_bound_criticals, index, criticals[i])
								end
								for i=#criticals+1,5 do
									local index = string.format("hit_%02d", i)
									if damage_bound_criticals[index].critical_type.critical ~= "" then
										print("\t\t\t[\""..index.."\"].critical_type.critical = \"\" (was: "..string.format("%s", tostring(damage_bound_criticals[index].critical_type.critical))..")")
										damage_bound_criticals[index].critical_type.critical = ""
									end
									if damage_bound_criticals[index].weight ~= 0 then
										print("\t\t\t[\""..index.."\"].weight = 0 (was: "..damage_bound_criticals[index].weight..")")
										damage_bound_criticals[index].weight = 0
									end
								end
								local i = math.max(6,#criticals+1)
								while true do
									local index = string.format("hit_%02d", i)
									if damage_bound_criticals[index] == nil then
										break
									end
									print("\t\t\t[\""..index.."\"] = nil (was: "..string.format("%s / %d", damage_bound_criticals[index].critical_type.critical, damage_bound_criticals[index].weight)..")")
									damage_bound_criticals[index] = nil
									i = i + 1
								end
							end

						end

					end
				end

				--rgd:save()
				break
			end
		end

	end
end

function at_end()
	print("Done!")
end
