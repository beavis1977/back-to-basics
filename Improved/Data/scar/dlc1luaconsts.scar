--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--
-- LUA CONSTANTS
-- Defines variables to be used for important Lua defined contsants
-- 
-- 
-- (c) 2008 Relic Entertainment
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

SBP.VILLERS_BOCAGE = {

	AXIS = 	{
		TIGER 				= BP_GetSquadBlueprint("sbps/races/axis/vehicles/villers_bocage_tiger_squad.lua"),
		REGULAR_TIGER		= BP_GetSquadBlueprint("sbps/races/axis/vehicles/villers_bocage_regular_tiger_squad.lua"),
		VOSS				= BP_GetSquadBlueprint("sbps/races/axis/soldiers/villers_bocage_voss_squad.lua"),
		CREW				= BP_GetSquadBlueprint( "sbps/races/axis/soldiers/villers_bocage_crew_squad.lua" ),
		PANZER_IV			= BP_GetSquadBlueprint( "sbps/races/axis/vehicles/villers_bocage_panzer_iv_squad.lua" ),
		SCHWIMMWAGEN		= BP_GetSquadBlueprint( "sbps/races/axis/vehicles/villers_bocage_schwimmwagen.lua" ),
	},
	
	CW = {
		MORTAR_MOBILE		= BP_GetSquadBlueprint("sbps/races/allies_commonwealth/soldiers/villers_bocage_mortar_section_mobile.lua"),
		M3_HALFTRACK		= BP_GetSquadBlueprint("sbps/races/allies/vehicles/villers_bocage_m3_halftrack_squad.lua"),
		VILLERS_TRUCK		= BP_GetSquadBlueprint("sbps/races/allies/vehicles/villers_bocage_deuce_and_a_half_cckw_squad.lua"),
	},
	
	ELITE = {
		FLAK_38				= BP_GetSquadBlueprint("sbps/races/axis_panzer_elite/vehicles/villers_bocage_flak38_aagun_squad.lua"),
	},
}

UPG.SP.VILLERS_BOCAGE = {

	COMMANDER_TREE		= BP_GetUpgradeBlueprint("upgrade/villers_bocage_rpg.lua"),
	VOSS1				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_voss1"),
	VOSS2				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_voss2"),
	VOSS3				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_voss3"),
	VOSS4				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_voss4"),
	VOSS5				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_voss5"),
	RADIO1				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_radio1"),
	RADIO2				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_radio2"),
	RADIO3				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_radio3"),
	DRIVER1				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_driver1"),
	DRIVER2				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_driver2"),
	DRIVER3				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_driver3"),
	GUNNER1				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_gunner1"),
	GUNNER2				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_gunner2"),
	GUNNER3				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_gunner3"),
	LOADER1				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_loader1"),
	LOADER2				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_loader2"),
	LOADER3				= BP_GetUpgradeBlueprint("upgrade/villers_bocage_loader3"),
	PIAT 				= BP_GetUpgradeBlueprint("upgrade/allies_cw/items/villers_bocage_piat_upgrade"),
	BOYS_RIFLE			= BP_GetUpgradeBlueprint("upgrade/allies_cw/items/commonwealth_boys_at_rifle"),
	
	FLAK_38_ANYWHERE	= BP_GetUpgradeBlueprint("upgrade/villers_bocage_no_territory_requirement.lua"),
	FIREFLY_SMOKE		= BP_GetUpgradeBlueprint("upgrade/villers_bocage_unlock_firefly_smoke.lua"),
	TIGER_ACE_SKIN		= BP_GetUpgradeBlueprint("upgrade/villers_bocage_change_tiger_skin.lua"),
	TIGER_BENGAL_SKIN	= BP_GetUpgradeBlueprint("upgrade/villers_bocage_bengal_tiger_skin.lua"),
	
}

ABILITY.SP.VILLERS_BOCAGE = {
	
	-- abilities specifically used by the Tiger Tank
	DIRECT_FIRE_HE  			= BP_GetAbilityBlueprint("abilities/villers_bocage_axis_tiger_direct_fire_he"),		-- the high explosive ability
	DIRECT_FIRE_PZGR  			= BP_GetAbilityBlueprint("abilities/villers_bocage_axis_tiger_direct_fire_pzgr"),		-- the Armor-Piercing Ability 
	DIRECT_FIRE_RIGHT_CLICK  	= BP_GetAbilityBlueprint("abilities/villers_bocage_axis_tiger_direct_fire_pzgr_right_default"),	-- The ability tied directly to the Right-mouse button
	DIRECT_FIRE_HE_DOUBLE		= BP_GetAbilityBlueprint("abilities/villers_bocage_axis_tiger_direct_fire_he_double"), 	--Upgraded when using double tap.
	DIRECT_FIRE_PZGR_DOUBLE		= BP_GetAbilityBlueprint("abilities/villers_bocage_axis_tiger_direct_fire_pzgr_double"), --Upgraded when using double tap.
	DIRECT_FIRE					= BP_GetAbilityBlueprint("abilities/toggle_direct_fire_tiger_villers_bocage"), -- THE ACTUAL DIRECT FIRE BUTTON
	TOGGLE_COMMANDER			= BP_GetAbilityBlueprint("abilities/villers_bocage_toggle_commander"),
	COMMANDER_UP				= BP_GetAbilityBlueprint("abilities/villers_bocage_commander_up"),
	COMMANDER_DOWN				= BP_GetAbilityBlueprint("abilities/villers_bocage_commander_down"),
	TIGER_REPAIR 				= BP_GetAbilityBlueprint("abilities/villers_bocage_self_repair.lua"),
	SLOW_TURRET					= BP_GetAbilityBlueprint("abilities/villers_bocage_slow_turret"),
	TIGER_SMOKE					= BP_GetAbilityBlueprint("abilities/viller_bocage_smoke_voss_tiger"),
	DEFENSIVE_SMOKE				= BP_GetAbilityBlueprint("abilities/villers_bocage_defensive_smoke"),
	ARTILLERY_BARRAGE			= BP_GetAbilityBlueprint("abilities/villers_bocage_artillery_strike_long"),
	FLANK_SPEED					= BP_GetAbilityBlueprint("abilities/villers_bocage_tiger_flank_speed"),
	TOGGLE_AMMO_HE				= BP_GetAbilityBlueprint("abilities/villers_bocage_ammo_he"),
	TOGGLE_AMMO_AP				= BP_GetAbilityBlueprint("abilities/villers_bocage_ammo_pzgr"),
	TANK_SHOCK					= BP_GetAbilityBlueprint("abilities/villers_bocage_tiger_tank_shock"),
	
	-- other abilities (crew, enemy units)
	FIREFLY_SMOKE				= BP_GetAbilityBlueprint("abilities/villers_bocage_smoke"),
	GRID_REFERENCE				= BP_GetAbilityBlueprint("abilities/villers_bocage_mark_strongpoint"),
	DEFENSIVE_WEAPON			= BP_GetAbilityBlueprint("abilities/villers_bocage_defensive_weapon"),
	CREW_HIDE					= BP_GetAbilityBlueprint("abilities/villers_bocage_hide_ability"),
	DROP_WEAPONS				= BP_GetAbilityBlueprint("abilities/sp/villers_bocage_drop_weapons"),
	CREW_SPRINT 				= BP_GetAbilityBlueprint("abilities/villers_bocage_sprint_ability"),
	CREW_ASSAULT				= BP_GetAbilityBlueprint("abilities/villers_bocage_assault_ability"),
	CREW_PANZERFAUST			= BP_GetAbilityBlueprint("abilities/villers_bocage_fire_panzerfaust"),
	CREW_SATCHEL				= BP_GetAbilityBlueprint("abilities/villers_bocage_satchel_charge"),
	CREW_MEDKIT					= BP_GetAbilityBlueprint("abilities/villers_bocage_medical_kit"),
	TYPHOON_ROCKET				= BP_GetAbilityBlueprint("abilities/air_strike_ability_rockets_villers_bocage"),
	
}

EBP.SP.VILLERS_BOCAGE = {
	FLAK_38_ANYWHERE 			= BP_GetEntityBlueprint("ebps/races/axis_panzer_elite/vehicles/villers_bocage_flakvierling_build_anywhere.lua"),
	SMOKE						= BP_GetEntityBlueprint("ebps/gameplay/smoke_cloud.lua"),
}

EBP.PICKUP.VILLERS_BOCAGE = {
	
	SATCHEL_CHARGE 				= BP_GetEntityBlueprint("ebps/gameplay/props/villers_bocage_ammobox_item.lua"),
	
	--*** COMMONWEALTH ***
	CW = {
		--infantry
		PIAT_HIGH_PENETRATION	= BP_GetEntityBlueprint("ebps/gameplay/props/villers_bocage_piat_item.lua"),
		BOYS_RIFLE				= BP_GetEntityBlueprint("ebps/gameplay/props/allied_boys_at_rifle_item.lua"),
	},
		
}

--*** SLOT ITEMS ***
-- (not to be confused with the "gameplay prop" EBP that slot items actually use)
SLOT_ITEM.VILLERS_BOCAGE =	{
	__scardoc_enum = true,
	
	SP = {
		TIGER_AMMO					= BP_GetSlotItemBlueprint("slot_item/villers_bocage_ammo_slot_item.lua"),
		COMMANDER_SLOT_ITEM         = BP_GetSlotItemBlueprint("slot_item/villers_bocage_commander_slot_item.lua"),
	},

	CW = {
		PIAT_HIGH_PENETRATION		= BP_GetSlotItemBlueprint("slot_item/villers_bocage_piat.lua"),
		BOYS_RIFLE					= BP_GetSlotItemBlueprint("slot_item/commonwealth_boys_at_rifle.lua"),
	},
}


ACTOR.VILLERS_BOCAGE = {
	Voss 				= "InGame\\portraits\\speech_pnze_voss",
	Schroif				= "InGame\\portraits\\speech_pnze_arno_portrait",
	Litzke 				= "InGame\\portraits\\speech_pnze_Litzke_portrait",
	Shultz 				= "InGame\\portraits\\speech_pnze_schultz_portrait",
	Berndt 				= "InGame\\portraits\\speech_pnze_Berndt_portrait",
	GenericWehrmacht	= "InGame\\portraits\\speech_axis_soldier",
}

MEDALS.VILLERS_BOCAGE = {

	MISSION_A			= BP_GetMedalBlueprint("medals/villers_bocage_a"),
	MISSION_B			= BP_GetMedalBlueprint("medals/villers_bocage_b"),
	MISSION_C			= BP_GetMedalBlueprint("medals/villers_bocage_c"),
	COMPLETION			= BP_GetMedalBlueprint("medals/villers_bocage_completion"),
}