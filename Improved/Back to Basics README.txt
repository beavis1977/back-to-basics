--------------------------------------------------

Back to Basics - a Company of Heroes modification

--------------------------------------------------

Mod Owners/Creators: AGameAnx & Celution

--------------------------------------------------

Module           = BackToBasics
ModName          = Back to Basics
ModVersion       = 5.0
ModFolder        = BackToBasics
LocaleFolder     = BackToBasics\Locale
ScenarioFolder   = BackToBasics\Data\Scenarios
TargetExtention  = -mod BackToBasics

--------------------------------------------------

Moddb link:

  http://www.moddb.com/mods/back-to-basics-battle-of-the-hedges

Please visit for updated versions of the mod, improved usage instructions and to provide feedback.
  We check the pages regularly and answer all the comments.

--------------------------------------------------

Installation instructions:
  
  1. Extract all files from the .rar archive that you downloaded to your main Company of Heroes
       folder (usually this is at C:\Program Files (x86)\Steam\SteamApps\common\Company of Heroes).
  
  2. Launch the Mod by using the provided shortcut called Back to Basics.
  
Uninstall instructions:

  To uninstall, simply remove all files you extracted into the Company of Heroes folder from
    this archive. The list of files extracted to the main folder is as follows:
    
    BackToBasics (folder)
    BackToBasics.module
    BackToBasicsDLC1.module
    BackToBasicsDLC2.module
    BackToBasicsDLC3.module
    BackToBasicsOPS.module

Additional installation notes:
  * To create your own steam shortcut:
    1. Make a shortcut of the game using steam
    2. Go to shotcut's properties (right click it, choose properties)
    3. Go to Shortcut menu in shortcut's properties
    4. Add //-mod BackToBasics to the end of the Target line. The entire target line should look like this:
         steam://rungameid/228200//-mod BackToBasics
  
  * To create a shortcut that doesn't show a prompt when launching the mod
    1. Go to your steam install directory (for example: C:\Program Files (x86)\Steam)
    2. Make a shortcut of Steam.exe
    3. (optional) Move the shortcut to your desktop, change its icon. A special mod icon can be located in BackToBasics folder of the mod.
    4. In shortcut properties add -applaunch 228200 -mod BackToBasics to the target line. The result should look something like this:
         "C:\Program Files (x86)\Steam\Steam.exe" -applaunch 228200 -mod BackToBasics
  
  * If you would like to be able to play any campaign mission without playing through all the
    campaign again, use a shortcut command -unlock_all_missions (add to shortcut target line)
  
  * Additional target line parameters include -novsync and -nomovies which disable game vsync (increases game's responsiveness and FPS) and intro movies respectively.

--------------------------------------------------

Tips about gameplay:

  1. Every weapon has the potential to deal a lot of damage up close, take good care of close quarters engagements and monitor units in combat closely.
  2. Squads under fire from multiple directions receive penalty modifiers. Protect your flanks.
  3. Clumped up infantry squads receive penalty modifiers. Make sure to spread out your infantry squads to maximize their effectiveness.
  4. Units slowly heal when out of combat, the heal rate is based on the total amount of health missing for each member.
  5. If you see a squad that has taken damage but not member losses, consider pulling it back from battle to reengage after it has healed up.
  6. Units in cover take reduced amount of damage and suppression than units in the open, the usage of cover is vital in battle.
  7. Short range weapons, like submachine guns and assault rifles, are very effective when used aggressively, but it takes planning to get them into close range.
  8. It is much cheaper to reinforce squads than to deploy a new one. Retreat your units in time to avoid taking unnecessary losses.
  9. Read the descriptions of officer units carefully, to get to know what they're best used for.
  10. Use smoke intelligently. Smoke can screen units from fire, offering excellent cover and protection against suppression.
  11. Choosing a proper commander tree progression is vital. Keep in mind that you aren't required to pick one right away and explore all the possibilities, as you'll find much diversity available.
  12. Most of the upgrades can only be purchased near Base Structures, Forward Barracks or supply providing vehicles such as the supply halftrack.
  13. Barraging with artillery costs a lot of munitions. Make sure to secure a stable munition income and position your barrages wisely.
  14. Denying munition income can be a viable way of preventing your enemy from using artillery barrages and other abilities against you.

--------------------------------------------------

Copyright:

  All content that is not originally made by CelÚstial and AGameAnx is used with permission and we
    do not claim any kind of credit for it. We want to thank all of the friendly people who allowed
    us to use their work in our modification. All rights are reserved to the owners and we cannot
    support further distribution of their work. If you want to use content that is not made by us,
    you will have to ask the original artist for permission.
  
  If we forgot someone, please let us now and his/her name will be added.
  
  Tankdestroyer & Team
    - Sd.Kfz. 250/9
    - Panther Tank Commander
    - Recoilless Jeep
    - Sd.Kfz. 251/7 Pioneer Halftrack
    - Churchill Tank Commander
  
  Halftrack & Tankdestroyer
    - Improved Jagdpanther
    - M5 76mm Anti-Tank Gun
    - Nashorn Tank Destroyer
    - M36 Jackson
    - Improved M26 Pershing
    - King Tiger Henschel Turret
    - M4 105mm Sherman
  
  BlackBishop
    - Marder III gun fix
    - Sd.Kfz. 250 series engine fix
  
  Eliw00d & DMz
    - Sd.Kfz. 251 visual slots
    - M3a1 Halftrack visual slots
    - M8 Scott
  
  DMz & Beefy^
    - Numerous small fixes in .rgo files
    - M10/M36 Sandbag armor and Stowage
    - Improved Firefly and Sherman V
    - M3 37mm Anti-Tank Gun (unused at this point, but files are included)
  
  Darkbladecr
    - Numerous small fixes in .rgo files
  
  VanAdrian:
    - Scopeless Gewehr 43 model
  
  MrScruff
    - Jagdtiger (unused at this point, but files are included)
  
  huetti07
    - German .ucs file
  
  maciag
    - Polish .ucs file
  
  Infantry Textures:
    - Halftrack
  
  Vehicle Textures:
    - GnigruH
    - SIG_21Surgeon
    - Xalibur
    - DMz
    - Widowmaker1
  
  Tracer Mod
    - I_am_a_Spoon
